var gulp = require('gulp');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');

var paths = {
  sass: ['./scss/**/*.scss'],
  controllers: ['./controllers/*.js'],
  services: ['./services/*.js']
};

gulp.task('default', ['sass']);

gulp.task('sass', function(done) {
  gulp.src('./scss/ionic.app.scss')
    .pipe(sass())
    .on('error', sass.logError)
    .pipe(gulp.dest('./css/'))
    .pipe(minifyCss({
      keepSpecialComments: 0
    }))
    .pipe(rename({ extname: '.min.css' }))
    .pipe(gulp.dest('./css/'))
    .on('end', done);
});

gulp.task('jsbuild', function () {
    gulp.src('./controllers/*.js')            
            .pipe(concat('controllers.js'))
            .pipe(gulp.dest('./js/'));
    
    gulp.src('./services/*.js')
            .pipe(concat('services.js'))
            .pipe(gulp.dest('./js/'));


});

gulp.task('watch', function() {
  gulp.watch(paths.sass, ['sass']);
  gulp.watch(paths.controllers, ['jsbuild']);
  gulp.watch(paths.services, ['jsbuild']);
});