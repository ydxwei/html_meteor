#########################################################################
# File Name: buildProduction.sh
# Author: ma6174
# mail: ma6174@163.com
# Created Time: 六  9/ 9 16:18:01 2017
#########################################################################
#!/bin/bash

git pull
meteor build ../user_client_production --architecture os.linux.x86_64
cd ../user_client_production
tar zxvf user_client_dev.tar.gz -C ./
rm user_client_dev.tar.gz
#(cd bundle/programs/server && npm install)
git add .
git commit -m 'add or fix user client'
git push
