function isWeiXin(){
		var ua = window.navigator.userAgent.toLowerCase();
		if(ua.match(/MicroMessenger/i) == 'micromessenger'){
		    return true;
		}else{
		    return false;
		}
}
function subToken(token,now,callback,callback1) {
	let selector = {
		'$and': [
			{
				'ttl': {
					'$gt': now
				}
			}, {
				'$or': [{
					token: token
				}, {
					md5: token
				}]
			}
		]
	}
	var self = this;
	Meteor.subscribe('loginTokens', selector, { limit: 1 }, function () {
		if (this.ready) {
			let token = LoginToken.findOne();
			if (!token) {
				delete localStorage.tokenData;
				delete localStorage.curUser;
				return callback1();
			} else {
				localStorage.tokenData = JSON.stringify(token);
				return callback();
			}
		}
	});
	return callback();
}

function isLoginedOrLogining(self) {
	if((typeof (self.params.query['token']) != 'undefined' &&
		self.params.query['token'] != null)){
			delete localStorage.tokenData;
			return true;
		}else if((typeof (localStorage.tokenData) != 'undefined' && localStorage.tokenData != null &&
			localStorage.tokenData != 'null') ){
				var tokenData = (JSON.parse(localStorage.tokenData));
				var time = (new Date()).getTime()/1000;
				if(tokenData.refresh_ttl > time && tokenData.ttl < time){
					self.params.query['token'] = tokenData.token;
					delete localStorage.tokenData;
					return true;
				}else if(tokenData.ttl > time ){
					return true;
				}else {
					delete localStorage.tokenData;
					return  false;
				}
		}
		return false
}
Router.route('/',function(){
	var queryStr = "?";
	for(var key in this.params.query){
		var str = key+'='+this.params.query[key];
		queryStr += (queryStr === "?" ? str : '&'+str);
	}
	this.stop();
	if(!isLoginedOrLogining(this)){
		if( isWeiXin() && (typeof this.params.query['third_party_id'] == 'undefined' ||
			this.params.query['third_party_id'] == null ) ){
			location.href = __meteor_runtime_config__['WEIXIN_AUTH_URL'];
		}else {
			return this.redirect('/login'+queryStr);
		}
	}else{
		 return this.redirect('/home'+queryStr);
	}
});
Router.route('/login',  {
  name: 'app.login',
  controller: 'LoginController'
});
Router.route('/user-center',  {
  name: 'user.center',
  controller: 'UserCenterController'
});
Router.route('/user-info',  {
  name: 'user.info',
  controller: 'UserInfoController'
});

Router.route('/newuser-info',  {
  name: 'newuser.info',
  controller: 'UserInfoController'
});

Router.route('/message',  {
  name: 'message',
  controller: 'messageController'
});
Router.route('/comment',  {
  name: 'comment',
  controller: 'commentController'
});
Router.route('/collection',  {
  name: 'collection',
  controller: 'CollectionController'
});
Router.route('/payment',  {
  name: 'payment',
  controller: 'paymentController'
});
Router.route('/card',  {
  name: 'card',
  controller: 'cardController'
});
Router.route('/card-detail/:id',  {
  name: 'card.detail',
  controller: 'cardDetailsController'
});
Router.route('/order',  {
  name: 'order',
  controller: 'orderController'
});
Router.route('/orderDetail/:id',  {
  name: 'order.detail',
  controller: 'orderDetailController'
});
Router.route('/merchantDetail/:id?',  {
  name: 'merchant.detail',
  controller: 'merDetailController'
});
Router.route('/merchant',  {
  name: 'merchant',
  controller: 'merController'
});
Router.route('/home',  {
	name: 'home',
	controller: 'HomeController'
});
Router.route('/region', {
	name: 'region',
	controller: 'RegionController'
});
Router.route('/gift/:id', {
	name: 'gift',
	controller: 'GiftController'
});

Router.route('/boss',  {
	name: 'boss',
	controller: 'bossController'
});
Router.route('/idle',  {
	name: 'idle',
	controller: 'idleController'
});
Router.route('/mdetail',  {
	name: 'mdetail',
	controller: 'mdetailController'
});
Router.route('/goods', {
  name: 'goods',
  controller: 'goodsController'
});
Router.route('/goodsDetail/:id', {
  name: 'goods.detail',
  controller: 'goodsDetailController'
});
Router.route('/goodsBuy', {
  name: 'goods.buy',
  controller: 'goodsBuyController'
});
Router.route('/merchantBuy', {
  name: 'merchant.buy',
  controller: 'merBuyController'
});

Router.route('/news', {
  name: 'news',
  controller: 'NewsController'
});
Router.route('/news/:id', {
  name: 'news.detail',
  controller: 'NewsDetailController'
});
Router.route('/orderComment', {
  name: 'orderComment',
  controller: 'OrderCommentsController'
});
Router.route('/messageDetail/:id', {
	name: 'messageDetail',
	controller: 'messageDetailController'
});

Router.route('/about-us', {
  name: 'about.us',
  controller: 'AboutUsController'
});
Router.route('/cooperate', {
  name: 'cooperate',
  controller: 'CooperateController'
});
Router.route('/refund/:id', {
  name: 'refund',
  controller: 'refundController'
});
Router.route('/paynotice',{
	name: 'paynotice',
  controller: 'paynoticeController'
});


Router.route('/merchantMap/:lng?/:lat?',{
	name: 'merchant.map',
  controller: 'merchantMapController'
});
