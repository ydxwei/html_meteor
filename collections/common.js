Ads = new Mongo.Collection('ads');

Categorys = new Mongo.Collection('categorys');

Cities = new Mongo.Collection('cities');

ClassCodes = new Mongo.Collection('classCodes');

Collections = new Mongo.Collection('collections');

Colleges = new Mongo.Collection('colleges');

Comments = new Mongo.Collection('comments');

Coupons = new Mongo.Collection('coupons');

CouponRecodes = new Mongo.Collection('couponRecodes');

Districts = new Mongo.Collection('districts');

Gifts = new Mongo.Collection('gifts');

goods = new Mongo.Collection('goods');

Majors = new Mongo.Collection('majors');

merchant = new Mongo.Collection('merchant');

merchantDetail = new Mongo.Collection('merchantDetail');

Messages = new Mongo.Collection('messages');

News = new Mongo.Collection('news');

OrderComments = new Mongo.Collection('orderComments');

Orders = new Mongo.Collection('orders');

OrderGoods = new Mongo.Collection('orderGoods');

Wallets = new Mongo.Collection('wallets');

Partners = new Mongo.Collection('partners');

Provinces = new Mongo.Collection('provinces');

Refunds = new Mongo.Collection('refunds');

Sites = new Mongo.Collection('sites');

Universities = new Mongo.Collection('universities');

Users = new Mongo.Collection('users');

Capitals = new Mongo.Collection('capitals');

Draws = new Mongo.Collection('draws');
