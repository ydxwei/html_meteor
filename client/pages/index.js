(function(context){
  function css(url){
    $("<link>")
    .attr({ rel: "stylesheet",
    type: "text/css",
    href: url
    })
    .appendTo("head");
  }
  function isPlatform(os){
    if(this.platform() == os)
      return true;
    return false;
  }
  $('head').ready(function(){
    if(isPlatform('iphone')){
      // <link rel="stylesheet" href="/packages/keanghok_framework7/framework7/dist/css/framework7.ios.min.css">
      // <link rel="stylesheet" href="/packages/keanghok_framework7/framework7/dist/css/framework7.ios.colors.min.css">
      css('/packages/keanghok_framework7/framework7/dist/css/framework7.ios.min.css');
      css('/packages/keanghok_framework7/framework7/dist/css/framework7.ios.colors.min.css');
    }else {
      css('/packages/keanghok_framework7/framework7/dist/css/framework7.material.min.css');
      css('/packages/keanghok_framework7/framework7/dist/css/framework7.material.colors.min.css');
    }
  });
  //this.platform = platform;
})(this);
