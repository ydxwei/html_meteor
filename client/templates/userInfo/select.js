Template.SchoolSelect.helpers({
	name:null,
	collection:null,
	next: [],
	selector:{},
	selectorReact:new ReactiveVar({}),
	subscribe:function(){

	},
	// list:function(){
	// 	return null;
	// },
  json:function(data){
    return JSON.stringify(data);
  }
});

Template.SchoolSelect.events({
  'click .f7-label-radio':function(e){
    let value = $(e.currentTarget).closest('li').data('item');
    this.submitCall(value);
    var myApp = new Framework7();
    var $$ = Dom7;
    myApp.closeModal($$('.' + this.name));
  }
});
Template.SchoolSelect.onRendered(function(){
	var myApp = new Framework7();
	var $$ = Dom7;
	var self = this;
	Tracker.autorun(function(){
		if(typeof self.selector != 'undefined')
				console.log(self.selector.get());
	});

	$$("."+this.name).on("open",function(){
		 //self.subscribe();
	});

	$$("."+this.name).on("close",function(){

	});
});
