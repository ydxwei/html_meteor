UserInfoController = BaseController.extend({
		template:'UserInfo',
	  center:'个人信息',
		mainView:'user-info-view',
		position:new ReactiveVar({
				province:{
						name:'省份'
				},
				city:{
						name:'城市'
				},
				district:{
						name:'区县'
				}
		}),
		waitOn:function(){
				let current = 'province';
				let self = this;
				this.current = new ReactiveVar(current);
				this.limit   = new ReactiveVar();
				this.defautprovince = {name:'省份（直辖市)'};
				this.province   = new ReactiveVar(this.defautprovince);
				this.defautcity = {name:'地级市'};
				this.city       = new ReactiveVar(this.defautcity);
				this.defautdistrict = {name:'区县'};
				this.district   = new ReactiveVar(this.defautdistrict);
				this.selectHeaderClass = new ReactiveVar([
					'province-header province-city-district',
					'city-header province-city-district',
					'district-header province-city-district']);
				this.selectName = new ReactiveVar(['province','city','district']);
				this.selectedVar = new ReactiveVar({
						province:new ReactiveVar('selected'),
						city:new ReactiveVar(),
						district:new ReactiveVar()
				});
				this.selectors={
						'cities':new ReactiveVar({province_code:null}),
						'districts':new ReactiveVar({city_code:null}),
						'universities':new ReactiveVar({district_code:null}),
						'colleges':new ReactiveVar({university_code:null}),
						'majors':new ReactiveVar({college_code:null,grade:null}),
						'classCodes':new ReactiveVar({major_code:null})
				};
				this.selectedItems = {
					province_code:null,
					city_code:null,
					district_code:null,
					university_code:null,
					college_code:null,
					grade:null,
					major_code:null,
					class_code:null
				};
				var user = null;
				self.newUserInfo = null;
				var routerName = Router.current().route.getName();
				Tracker.autorun(function(){
					if(!user){
						user = self.currentUser();
						//self.setUser(user);
					}

					self.subscribeProinces();
					if(routerName != "user.info" ){
							self.userInfoNoExist = true;
							if(!self.newUserInfo)
								self.newUserInfo = _.extend({},{
									province_code:!user ? null : user.province_code,
									city_code:!user ? null : user.city_code,
									district_code:!user ? null : user.district_code,
									university_code:!user ? null : user.university_code,
									college_code:!user ? null : user.college_code,
									grade:!user ? null : user.grade,
									major_code:!user ? null : user.major_code,
									class_code:!user ? null : user.class_code,
									class:!user ? null : user.class
								});
					}
					if(!user || typeof user == 'undefined')
						return ;
					self.subscribeCities(user.province_code);
					//if(self.selectors['cities']['city_code']){
					self.subscribeDistricts(user.city_code);
					//}

					//if(uself.selectors['districts']['district_code']){
					self.subscribeUniversity(user.district_code);
					//}

					//if(self.selectors['universities']['university_code']){
					self.subscribeCollege(user.university_code);
					//}

					//if(self.selectors['colleges']['college_code'] && elf.selectors['grade']){
					self.subscribeMajor(user.college_code,user.grade);
					//}

					//if(self.selectors['majors']['major_code']){
					self.subscribeClassCode(user.major_code);

				});
		},
		subscribeProinces(){
			 Meteor.subscribe('provinces',{},{sort:{first_word:1}});
		},
		subscribeCities(provinceCode){
			 var selector = this.selectors['cities'].get();
			 //console.log(selector);
			 //console.log(this.selectedItems['province_code']);
			 if(!selector['province_code'] && !!provinceCode){
				    selector['province_code'] = provinceCode;
			 }
			 if(!!selector['province_code']  ){
			  	 Meteor.subscribe('cities',selector,{sort:{first_word:1}});
			 }
		},
		subscribeDistricts(cityCode){
			 var selector = this.selectors['districts'].get();
			 if(!selector['city_code'] && !!cityCode){
				   selector['city_code'] = cityCode;
			 }
			 if( !!selector['city_code'] ) {
				   this.selectedItems['city_code'] = selector['city_code'];
				   Meteor.subscribe('districts',selector,{sort:{first_word:1}});
			 }
		},
		subscribeClassCode(majorCode){
			var selector = this.selectors['classCodes'].get();
			if(!selector['major_code'] && !!majorCode){
					selector['major_code'] = majorCode;
			}
			if(!!selector['major_code'] ) {
				  this.selectedItems['major_code'] = selector['major_code'];
					var _selector ={$or:[{'major_code':parseInt(selector['major_code'])},
					{'major_code':( selector['major_code'] ? selector['major_code'].toString() : null)}]};
					Meteor.subscribe('classCodes',_selector,{sort:{'class':1}});
			}
		},
		subscribeMajor(collegeCode,grade){
			var selector = this.selectors['majors'].get();
			if(!selector['college_code'] && !!collegeCode){
					selector['college_code'] = collegeCode;
			}
			if(!selector['grade'] && !!grade){
					selector['grade'] = grade;
			}
			if( !!selector['grade'] && !!selector['college_code']){
				  this.selectedItems['college_code'] = selector['college_code'];
					this.selectedItems['grade'] = selector['grade'];
					var _selector ={$and:[
																{$or:[
																	{'college_code':parseInt(selector['college_code'])},
																	{'college_code': (selector['college_code'] ? selector['college_code'].toString() : null)}
																 ]
																},
																{$or:[
																	{'grade':parseInt(selector['grade'])},
																	{'grade':(selector['grade'] ? selector['grade'].toString() : null)}
																]
															}
														]
													};
					Meteor.subscribe('majors',_selector,{sort:{id:1}});
			}
		},
		subscribeCollege:function(universityCode){
			var selector = this.selectors['colleges'].get();
			if(!selector['university_code'] && !!universityCode){
					selector['university_code'] = universityCode;
			}
			if( !!selector['university_code']  ) {
				  this.selectedItems['university_code'] = selector['university_code'];
					var _selector ={$or:[{'university_code':parseInt(selector['university_code'])},
					{'university_code':(selector['university_code'] ? selector['university_code'].toString() : null)}]};
					Meteor.subscribe('colleges',_selector,{},{sort:{id:1}});
			}
		},
		subscribeUniversity:function(districtCode){
			var selector = this.selectors['universities'].get();
			if(!selector['district_code'] && !!districtCode){
					selector['district_code'] = districtCode;
			}
			if( !!selector['district_code'] ){
				  this.selectedItems['district_code'] = selector['district_code'];
				  Meteor.subscribe('universities',selector,{},{sort:{id:1}});
			}
		},
		updateUser:function(data,back){
			let self = this;
			if(typeof(back) != 'undefined'){
				var isStudents = !!data['mark_info'];
				var fillStudents = ( data['province_code'] && data['city_code'] && data['district_code'] &&
				data['university_code'] && data['college_code'] && data['grade'] && data['major_code']
				&& data['class_code'] ) ;
				if( !isStudents && !fillStudents ){
					var myApp = new Framework7({
				   modalTitle: "提示",
				   modalButtonOk: "确定",
				   modalButtonCancel: "取消",
				  });
					myApp.alert('请填写详细的班级信息或者填写好备注才能提交');
					return;
				}
			}
			this.put('user/user/'+this.curUser.id,data).then(function(res){
				if(res.data.state == "OK" ){
					typeof(back) != 'undefined' ? self.router.go('home') : '';
					localStorage.curUser = JSON.stringify(self.curUser);
				}
			},function(){

			});
		},
		events:function(){
			let self = this;
			return {
				'click .submit-btn .button':function(){
					console.log(self.newUserInfo);
					self.newUserInfo['mark_info'] = $("#user_mark_info").val();
					self.updateUser(self.newUserInfo,'home');
				}
			}
		},
		data:function(){
			let self = this;
			return {
				user:function(){
					return self.currentUserData.get();
				},
				username:function(user){
					let name = '';
					if(user !=null && typeof(user) != 'undefined')
					 name = user.nick_name ? user.nick_name :(user.user_name ? user.user_name : user.mobile);
					return name;
				},
				avatar:function(avatar){
					if(avatar != null && typeof(avatar) != 'undefined'){
						return avatar.path;
					}else{
						return '';
					}
				},
				sex:function(s){
					if(!s)
						 return '请选择性别';
					let sex={
						male:'男',
						female:'女'
					};
						return sex[s];
				},
				hasNoUserInfo:function(user){
					return self.userInfoNoExist;
				},
				hasUserInfo:function(user){
					return !self.userInfoNoExist;
				},
				markShow:function(){
					return self.userInfoNoExist?'mark-show':'';
				},
				location:function(user){
					if(!user||!(user.province_name || user.city_name || user.district_name)){
						return '选择所在城市';
					}else{
						return user.province_name+(user.city_name?(('/'+user.city_name)+(user.district_name?('/'+user.district_name):'')):'');
					}
				},
				popups:[
					{
						template:'SchoolSelect',
						data:{
							submitCall:function(params){
								let data ={};
								data['university_code'] = self.curUser.university_code = params.code;
								data['university_name'] = self.curUser.university_name = params.name;
								data['college_code'] = self.curUser.college_code = null;
								data['college_name'] = self.curUser.college_name = null;
								data['grade'] = self.curUser.grade = null;
								data['major_code'] = self.curUser.major_code = null;
								data['major_name'] = self.curUser.major_name = null;
								data['class_code'] = self.curUser.class_code = null;
								data['class'] = self.curUser.class = null;
								self.currentUserData.set(self.curUser);
								var selector = self.selectors['colleges'].get();
								selector['university_code'] = data['university_code'];
								self.selectors['colleges'].set(selector);
								console.log(self.userInfoNoExist);
								if(!self.userInfoNoExist){
									self.updateUser(data);
								}else{
									self.newUserInfo = _.extend(self.newUserInfo,data);
								}
							},
							list:function(){
								//console.log(self.selectors['universities'].get());
								var selector = self.selectors['universities'].get();
								return Universities.find(selector).fetch();
							},
							name: "popup-university",
							collection: Universities,
							selector:self.universitySelect
						}
					},
					{
						template:'SchoolSelect',
						data:{
							submitCall:function(params){
								let data ={};
								data['college_code'] = self.curUser.college_code = params.code;
								data['college_name'] = self.curUser.college_name = params.name;
								data['grade'] = self.curUser.grade = null;
								data['major_code'] = self.curUser.major_code = null;
								data['major_name'] = self.curUser.major_name = null;
								data['class_code'] = self.curUser.class_code = null;
								data['class'] = self.curUser.class = null;
								self.currentUserData.set(self.curUser);
								var selector = self.selectors['majors'].get();
								selector['college_code'] = data['college_code'];
								self.selectors['majors'].set(selector);
								if(!self.userInfoNoExist){
									self.updateUser(data);
								}else{
									self.newUserInfo = _.extend(self.newUserInfo,data);
								}
							},
							list:function(){
								var selector = self.selectors['colleges'].get();
								return Colleges.find({$or:[
									{'university_code':(selector['university_code'] ? selector['university_code'].toString() : null)},
									{'university_code':parseInt(selector['university_code'])}
								]}).fetch();
							},
							name: "popup-college",
							collection: Colleges,
						}
					},
					{
						template:'SchoolSelect',
						data:{
							submitCall:function(params){
								let data ={};
								data['grade'] = self.curUser.grade = params.name;
								data['major_code'] = self.curUser.major_code = null;
								data['major_name'] = self.curUser.major_name = null;
								data['class_code'] = self.curUser.class_code = null;
								data['class'] = self.curUser.class = null;
								var selector = self.selectors['majors'].get();
								selector['grade'] = data['grade'];
								self.selectors['majors'].set(selector);
								self.currentUserData.set(self.curUser);
								if(!self.userInfoNoExist){
									self.updateUser(data);
								}else{
									self.newUserInfo = _.extend(self.newUserInfo,data);
								}
							},
							list:function(){
								return [{name:2017},{name:2016},{name:2015},{name:2014},{name:2013},
									{name:2012},{name:2011},{name:2010}];
							},
							name: "popup-grade"
						}
					},
					{
						template:'SchoolSelect',
						data:{
							submitCall:function(params){
								let data ={};
								data['major_code'] = self.curUser.major_code = params.code;
								data['major_name'] = self.curUser.major_name = params.name;
								data['class_code'] = self.curUser.class_code = null;
								data['class'] = self.curUser.class = null;
								self.currentUserData.set(self.curUser);
								var selector = self.selectors['classCodes'].get();
								selector['major_code'] = data['major_code'];
								self.selectors['classCodes'].set(selector);
								if(!self.userInfoNoExist){
									self.updateUser(data);
								}else{
									self.newUserInfo = _.extend(self.newUserInfo,data);
								}
							},
							list:function(){
								var selector = self.selectors['majors'].get();
								return Majors.find({$and:[
																			{$or:[
																				{'college_code':parseInt(selector['college_code'])},
																				{'college_code':(selector['college_code'] ? selector['college_code'].toString() : null)}
																			 ]
																			},
																			{$or:[
																				{'grade':parseInt(selector['grade'])},
																				{'grade':(selector['grade'] ? selector['grade'].toString() : null)}
																			]
																		}
																	]
																}).fetch();
							},
							name: "popup-major",
							collection: Majors,
						}
					},
					{
						template:'SchoolSelect',
						data:{
							submitCall:function(params){
								let data ={};
								data['class_code'] = self.curUser.class_code = params.code;
								data['class'] = self.curUser.class = params.name;
								self.currentUserData.set(self.curUser);
								if(!self.userInfoNoExist){
									self.updateUser(data);
								}else{
									self.newUserInfo = _.extend(self.newUserInfo,data);
								}
							},
							list:function(){
								var selector = self.selectors['classCodes'].get();
								return ClassCodes.find({$or:[
										{'major_code':(selector['major_code'] ? selector['major_code'].toString() : null)},
										{'major_code':parseInt(selector['major_code'])}
								]}).fetch();
							},
							name: "popup-class-code",
							collection: ClassCodes,
						}
					},
					{
						template:'SexSelect',
						data:{
							submitCall:function(sex){
								let data ={};
								data['sex'] = self.curUser.sex = sex;
								self.currentUserData.set(self.curUser);
								if(!self.userInfoNoExist){
									self.updateUser(data);
								}else{
									self.newUserInfo = _.extend(self.newUserInfo,data);
								}
							}
						}
					},
					{
						template:'SelectComponent',
						data:{
							popupClassName:'area-selector',
							subscribe:function(callback){
									this.subscribeHandle = Meteor.subscribe('provinces',{},{sort:{first_word:1}},callback);
							},
							headerItems:new ReactiveVar([
								{
									headerClass:function(){
											return self.selectHeaderClass.get()[0];
									},
									name:function(){
											return self.selectName.get()[0];
									},
									context:function(){
											return self.province.get();
									},
									currentReactive:self.current,
									selectedVar:self.selectedVar,
									default:self.defautprovince
								},{
									headerClass:function(){
											return self.selectHeaderClass.get()[1];
									},
									name:function(){
											return self.selectName.get()[1];
									},
									context:function(){
											return self.city.get();
									},
									currentReactive:self.current,
									selectedVar:self.selectedVar,
									default:self.defautcity
								},{
									headerClass:function(){
											return self.selectHeaderClass.get()[2];
									},
									name:function(){
											return self.selectName.get()[2];
									},
									context:function(){
											return self.district.get();
									},
									currentReactive:self.current,
									selectedVar:self.selectedVar,
									default:self.defautdistrict
								}
							]),
							collection:Provinces,
							currentReactive:self.current,
							selectListItems:new ReactiveVar([
								{
									name:'province',
									next:'city',
									collection:Provinces,
									current:self.current,
									list:function(){
										//console.log('province list');
										return Provinces.find({}).fetch();
									},
									nextSub:function(code){
										//console.log(code);
										var selector = self.selectors['cities'].get();
										selector['province_code'] = code;
										self.selectors['cities'].set(selector);
									},
									subscribeOptions:{sort:{first_word:1}},
									currentData:self.province,
									selectedVar:self.selectedVar,
									default:self.defautprovince,
									events:{
										'trigger':'selected_province',
										'key':'province_code',
										'initLoad':true
									},
									parent:null,
									selector:new ReactiveVar({}),
									selected:self.provinceSelect
								},{
									name:'city',
									next:'district',
									collection:Cities,
									current:self.current,
									selectedVar:self.selectedVar,
									default:self.defautcity,
									list:function(){
										//console.log('city list');
										return Cities.find(self.selectors['cities'].get()).fetch();
									},
									nextSub:function(code){
										var selector = self.selectors['districts'].get();
										selector['city_code'] = code;
										self.selectors['districts'].set(selector);
									},
									subscribeOptions:{sort:{first_word:1}},
									currentData:self.city,
									parent:self.province,
									events:{
										'trigger':'selected_city',
										'on':'selected_province',
										'key':'city_code',
										'initLoad':false
									},
									selector:self.provinceSelect,
									selected:self.citySelect
								},{
									name:'district',
									next:'province',
									collection:Districts,
									list:function(){
										return Districts.find(self.selectors['districts'].get()).fetch();
									},
									nextSub:function(code){
										var selector = self.selectors['universities'].get();
										selector['district_code'] = code;
										//console.log(selector);
										self.selectors['universities'].set(selector);
									},
									closeModule:function(callback){
											var myApp = new Framework7();
											var $$ = Dom7;
											myApp.closeModal($$('.area-selector'));
											let province = self.province.get(),
											city = self.city.get(),
											district = self.district.get();
											let data = {};
											data['province_code'] = self.curUser.province_code = province.code;
											data['province_name'] = self.curUser.province_name = province.name;
											data['city_code'] = self.curUser.city_code     = city.code;
											data['city_name'] = self.curUser.city_name     = city.name;
											data['district_code'] = self.curUser.district_code = district.code;
											data['district_name'] = self.curUser.district_name = district.name;

											data['university_code'] = self.curUser.university_code = null;
											data['university_name'] = self.curUser.university_name = null;
											data['college_code'] = self.curUser.college_code = null;
											data['college_name'] = self.curUser.college_name = null;
											data['grade'] = self.curUser.grade = null;
											data['major_code'] = self.curUser.major_code = null;
											data['major_name'] = self.curUser.major_name = null;
											data['class_code'] = self.curUser.class_code = null;
											data['class'] = self.curUser.class = null;
											self.currentUserData.set(self.curUser);
											if(!self.userInfoNoExist){
												self.updateUser(data);
											}else{
												self.newUserInfo = _.extend(self.newUserInfo,data);
											}
									},
									current:self.current,
									selectedVar:self.selectedVar,
									default:self.defautdistrict,
									currentData:self.district,
									parent:self.city,
									events:{
										'initLoad':false
									},
									selected:self.districtSelect,
									selector:this.citySelect
								},
							])
						}
					}
				]
			}
		}
})
