HomeController = BaseController.extend({
	template: 'Home',
	headerTransparent: true, 
	left: '<a href="/region" class="link icon-only f7-top-left external"><span>' + (localStorage.siteName ? localStorage.siteName : '区域') + '</span><i class="f7-icons size-34">down</i></a>',
	right: '<a href="/merchant"  class="external"><div class="f7-search"><i class="f7-icons s7-size-16">search</i> <span class="f7-search-input">更多优惠商家</span></div> </a><a href="/message" class="f7-top-icon external"><i class="iconfont icon-xiaoxi"></i></a>',
	bottomShow: new ReactiveVar(false),
	merchantsub: null,
	ntime: new ReactiveVar(),
	sid:new ReactiveVar(0),
	waitOn: function () {
		this.limit = 3;
		let self = this;
		self.selector = {};
		if (localStorage.lat && localStorage.lng ) {
			this.slat = parseFloat(localStorage.lat);
			this.slng = parseFloat(localStorage.lng);
			self.selector= {location:{$near:{$geometry:{type:'Point',coordinates:[self.slng,self.slat]}}},state: 'opened'};
		}else{
			this.slat = 31.785381;
			this.slng = 117.242346;
		}
		self.selectorSite = {};
		let site_id = localStorage.siteId;
		if (site_id) {
			self.sid.set(site_id);
			self.selectorSite = {
				site_ids: parseInt(site_id)
			};
		}
		Tracker.autorun(function () {
			self.bottomShow.set(self.showBottom());
			self.get('user/get-time',{}).then(function(res){
				self.ntime.set(res.data);
			}, function (err) {
				console.log(err)
			})
			self.merchantsub = Meteor.subscribe('merchant', self.selector,{
				limit: 15,
				fields:{
					list_image:true,
					id:true,
					name:true,
					month_order_count:true,
					labels:true,
					cate:true,
					site_name:true,
					idle:true,
					coupons:true,
					idles:true,
					idle_day:true,
					location:true,
					cate_id:true,
					site_ids:true,
					sites:true
				}
			});
			return [
				Meteor.subscribe('ads', self.selectorSite, {fields:{
					links:true,
					imgs:true
				}}),
				Meteor.subscribe('categorys', self.selectorSite, {fields:{
					name:true,
					img:true,
					id:true
				}}),
				Meteor.subscribe('gifts', self.selectorSite, {fields:{
					name:true,
					poster_img:true,
					id:true
				}}),
				self.merchantsub,
				Meteor.subscribe('news', {}, {
					sort: {id: -1},
					limit: 2,
					fields:{
						title:true,
						id:true
					}
				}),
			];
		});
		if(this.params.query['authinfo']){
			var token = JSON.parse(this.params.query['authinfo']);
			LoginToken.upsert({id:token['id']},token);
		}
	},
	onRendered: function (controller) {
		let self = this;

		$(document).on('touchmove', function (e) { //绑定手机滑屏事件
			let offset = $("#category").offset();
			if (offset == null || typeof (offset) == 'undefined')
				return;
			let TopVaule1 = offset.top;
			if (TopVaule1 < 0) {
				$(".navbar").removeClass('transparent');
				$(".navbar").removeClass('mask');
			} else {
				$(".navbar").addClass('transparent');
				$(".navbar").addClass('mask');
			}
		})
		var index = 0;
		//3秒轮播一次
		this.topLineTimer = setInterval(function () {
			index = (index == 3) ? 0 : index + 1;
			$(".f7-headline-content").scrollTop(22 * index);

		}, 3000);

		setTimeout(function(){
			var myApp = new Framework7();
			var plug = myApp.swiper('.swiper-slow', {
				pagination: '.swiper-slow .swiper-pagination',
				spaceBetween: 0,
				speed: 300
			});

			let adLength = $('.f7-ad .swiper-slide').length;
			if(adLength<2){
				$('.f7-ad > .swiper-pagination').hide();
			}
			let categoryLength = $('.f7-category .swiper-slide').length;
			if(categoryLength<2){
				$('.f7-category > .swiper-pagination').hide();
			}
			let giftLength = $('.f7-gift .swiper-slide').length;
			if(giftLength<2){
				$('.f7-gift .swiper-pagination').hide();
			}
		},1000)

	},
	routeStop: function () {
		this.merchantsub.stop();
		clearInterval(this.topLineTimer);
	},
	onStop: function () {
		this.merchantsub.stop();
		clearInterval(this.topLineTimer);
	},
	data: function () {
		let self = this;
		return {
			scroll: 'infinite-scroll',
			merList: function () {
				var data = merchant.find().fetch();
				var t = self.ntime.get();
				if(t != 'undefined' && t != undefined){
				for (var i in data) {
					var loc = self.gtlng(self.slat, self.slng, data[i]['location']['coordinates'][1],data[i]['location']['coordinates'][0]);
					if (Math.round(loc / 1000 * 100) / 100 > 1000) {
						data[i].jl = '';
					} else {
						data[i].jl = Math.round(loc / 1000 * 100) / 100 + 'km';
					}
					if(data[i].sites){
						var ssid = self.sid.get();
						for(var g in data[i].sites){
							if(typeof(data[i].sites[g]) != 'undefined' && typeof(data[i].sites[g].name) != 'undefined'){
								data[i].site_name = data[i].sites[g].name;
								if(ssid){
									if(ssid == data[i].sites[g].id){
										data[i].site_name = data[i].sites[g].name;
										break;
									}
								}
							}
						}
					}
					data[i].idle = {};
					for(var j in data[i].idles){
						if(typeof(data[i].idles[j].idle) != 'undefined' && data[i].idles[j].d == t.w){
							for(var k in data[i].idles[j].idle){
								if(typeof(data[i].idles[j].idle[k].start_second) != 'undefined'){
									if(data[i].idles[j].idle[k].start_second < t.t && data[i].idles[j].idle[k].end_second > t.t){
										data[i].idle = data[i].idles[j].idle[k];
									}
								}
							}
						}
					}
				}
			}
				return data;
			},
			visitor: function () {
				return !!localStorage.tokenData ? '' : 'f7-home-merchant-visitor';
			},
			adList: function () {
				return Ads.find().fetch();
			},
			categoryList: function () {
				let categorys = Categorys.find({},{sort:{id:-1}}).fetch();
				let adds = [];
				if (categorys.length > 0) {
					let k = 0;
					for (var i in categorys) {
						if (typeof (categorys[i].img) != 'undefined') {
							if (categorys[i].img.path) {
								let num = Math.floor(k / 4);
								if (adds[num]) {
									adds[num].cat.push(categorys[i]);
								} else {
									adds[num] = {
										cat: [categorys[i]]
									};
								}
								k++;
							}
						}
					}
				}
				return adds;
			},
			giftList: function () {
				return Gifts.find().fetch();
			},
			newsList: function () {
				return News.find().fetch();
			}
		}
	},
})
