GiftController = BaseController.extend({
    template:'Gift',
    headerTransparent:false,
    left: '新用户礼包',
    mainView:'Gift',
	waitOn: function () {
        let self = this;
        self.is_coupon = [];
        Tracker.autorun(function(){
            self.user = self.currentUser();
            if(self.user){
                console.log(self.user.id)
                Meteor.subscribe('couponRecodes',{user_id: self.user.id},{});
                self.isCoupon();
            }
            return Meteor.subscribe('gifts',{id:parseInt(self.params['id'])},{});
        });
    },
	onRendered:function (controller) {
        let self = this;
    },
	data:function () {
        let self = this;
		return{
            gift:function () {
                let gift = Gifts.findOne();
                if(gift){
                    Meteor.subscribe('coupons',{id: {$in: gift.coupon_ids}, state: 'enable'},{});
                    gift.coupons = Coupons.find().fetch();

                    if (gift.coupons.length>0 && self.is_coupon.length>0) {
                        for(var i in gift.coupons){
                            if (self.is_coupon.indexOf(gift.coupons[i]['id'])>-1) {
                                gift.coupons[i]['is_coupon'] = true;
                            }else{
                                gift.coupons[i]['is_coupon'] = false;
                            }
                        }
                    }
                }
                console.log(gift);
                return gift;
            }
        }
    },
    isCoupon: function() {
        let self = this;
        let data = CouponRecodes.find({user_id: self.user.id},{fields:{'coupon_id':1}}).fetch();
        if (data && data.length>0){
            for(var i in data){
                self.is_coupon.push(data[i]['coupon_id']);
            }
        }
        return self.is_coupon;
    },
    events:function (controller) {
        let self = this;
        return{
            'click #button':function(e) {
                if(self.user){
                    let coupon = self.data().gift();
                    if (coupon && coupon.coupon_ids) {
                        let coupon_ids = coupon.coupon_ids.join(',');
                        console.log(coupon_ids)
                        if (coupon_ids) {
                            self.postCoupon(controller,self.user.id,coupon_ids,"#button");
                        }
                    }  
                }else{
                    alert('请先登录')
                }
            },
            'click #selected':function(e) {
                if(self.user){
                    console.log(e)
                    let couponId = e.target.id;
                    self.postCoupon(controller,self.user.id, couponId);
                }else{
                    alert('请先登录')
                }
            },
        }
    },
    postCoupon: function (controller,userId,couponIds,type) {
        if (couponIds) {
            controller.post('user/coupon-recode/'+ userId, {coupon_ids:couponIds})
                .then(function (res) {
                    if (res.data && ('OK' == res.data.state)) {
                        for (var i in couponIds) {
                            $("#"+ couponIds[i]).html('已领取');
                            $("#"+ couponIds[i]).attr("disabled",true); 
                        }
                        if(type){
                            $("#button").html('已全部领取');
                            $("#button").attr("disabled",true); 
                        }
                    }
            },function (error) {
                controller.app.showPreloader('领取失败')
                setTimeout(function () {
                    controller.app.hidePreloader();
                }, 2000);
            })
        }
        
                
    }

})
