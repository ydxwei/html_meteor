cardController = BaseController.extend({
	template:'card',
    center:'优惠券',
	mainView: 'card-view',
	waitOn: function () {
        let self = this;
        Tracker.autorun(function(){
            self.user = self.currentUser();
            console.log(self.user);
            if(self.user !='null'){
                console.log(self.user.id);
                return Meteor.subscribe('couponRecodes',{user_id: parseInt(self.user.id)},{});
            }
        })
    },
	data:function () {
        let self = this;
		return{
            couponList:function () {
                return CouponRecodes.find().fetch();
            }
        }
    },
});

cardDetailsController = BaseController.extend({
	template: 'cardDetails',
	center: '优惠券详情',
	waitOn: function () {
        console.log(this.params['id'])
        let self = this;
		Tracker.autorun(function(){
            return Meteor.subscribe('coupons',{id: parseInt(self.params['id'])},{});
        });
    },
	onRendered:function (controller) {
        let self = this;
    },
    
	data:function () {
        let self = this;
		return{
            coupon:function () {
                return Coupons.findOne();
            }
        }
    },
    events:function (controller) {
        let self = this;
        return{
        }
    },
})
