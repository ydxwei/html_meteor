InitedController= BaseController.extend({
  template:'inited',
  headerHidden:true,
  mainView:'init-main-view',
  waitOn:function(){
    this.timeouter = new ReactiveVar(3);
    this.accessToken()
    this.loginToken();
    this.siteGeo();
  },
  data:function(){
    let self = this;
      return {
        timeout:function(){
            return self.timeouter.get();
        }
      }
  },
  onRendered:function(controller){
    $('.android.init-main-view .page-content .inited-content').css('top','-44px');
    $('.android.init-main-view .page-content .inited-content').css('padding-bottom','44px');
    let count = controller.timeouter.get();
    //$('.init-main-view .count-time>span').html(count);
    let id = setInterval(function () {
      //controller.router.go('app.login',{},{host:controller.originalUrl});
      if( count-- ){
        controller.timeouter.set(count);
        //$('.init-main-view .count-time>span').html(count);
        //clearTimeout(id);
      }else{
        clearInterval(id);
        controller.router.go('app.login',{},{host:controller.originalUrl});
      }
    }, 1000);
    controller.get('auth/app-sign/'+CryptoJS.MD5(controller.apiKey+controller.apiSecret).toString()).
    then(function(res){
      let data = res.data;
      if(data.state == 'OK'){
          controller.get('auth/app-code',{app_key:controller.apiKey,timestamp:data.data.timestamp,sign:data.data.sign})
          .then(function(res){
            if(res.data.state == 'OK'){
              controller.get('auth/access-token/'+encodeURIComponent(res.data.data.code))
              .then(function(res){
                if(res.data.state == "OK"){
                  localStorage.accessToken = res.data.data.token;
                //  console.log(res.data);
                  AccessToken.insert(res.data.data);
                }

              },function(res){

              });
            }
          },function(error){

          });
      }

    },function(error){

    });
  }
});
