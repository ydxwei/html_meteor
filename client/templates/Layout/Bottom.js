Template.BottomToolbar.events({
  'click a.user-center':function(){
    return Router.go('user.center');
  },
  'click a.home':function(){
    return Router.go('home');
  },
  'click a.order':function(){
    return Router.go('order');
  }
});

Template.BottomToolbar.helpers({
  selected(route){
    let name = Router.current().route.getName();
    if(route == name){
      return 'selected';
    }else{
      return '';
    }
  }
});
