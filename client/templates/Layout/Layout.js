Template.Layout.helpers({
  bottomShow:function(){
    let controller = Iron.controller();
    return typeof controller != "undefined" && typeof controller.bottomShow != 'undefined' && !!controller.bottomShow.get();
  },
  platform:function(os){
    let controller = Iron.controller();
    return typeof controller != "undefined" && typeof controller.platform != 'undefined' && controller.platform() == os;
  },
  mainView:function(){
    let controller = Iron.controller();
    return typeof controller != "undefined" && controller.mainView;
  },
  host:function(){
    let controller = Iron.controller();
    return typeof controller != "undefined" && controller.originalUrl;
  },
  popups:function(){
    let controller = Iron.controller();
    return typeof controller != "undefined" && typeof controller.data != 'undefined' && controller.data().popups;
  }
});
