Template.Header.helpers({
  url:function(){
    if(!Template.Header.url )
      return '';
    return Template.Header.url.get();
  },
  center:function(){
    if(!Template.Header.center )
      return '';
    return Template.Header.center.get();
  },
  left:function(){
    if(!Template.Header.left )
      return '';
    return Template.Header.left.get();
  },
  isHtml:function(htmlStr){
    var  reg = /<[^>]+>/g;
    return reg.test(htmlStr);
  },
  rightHidden:function(){
    let controller = Iron.controller();
    return controller.right == ''? 'header-hidden' : '';
  },
  right:function(){
    if(!Template.Header.right )
      return '';
    return Template.Header.right.get();
  },
  hidden:function(){
    let controller = Iron.controller();
    return !!controller.headerHidden ? 'hidden' : '';
  },
  transparent:function(){
    if(!Template.Header.transparent )
      return '';
    return Template.Header.transparent.get();
  }
});
Template.Header.left = new ReactiveVar();
Template.Header.right = new ReactiveVar();
Template.Header.center = new ReactiveVar();
Template.Header.url = new ReactiveVar();
Template.Header.transparent = new ReactiveVar();
Template.Header.onRendered(function(){

});

Template.Header.events({
	'click #go_page': function(){
		history.go(-1);
	}
});
