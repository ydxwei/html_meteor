Template.SelectComponent.helpers({
  headerItems:null,
  headers:function(){
    return this.headerItems ? this.headerItems.get() : [];
  },
  selectList:function(){
    return this.selectListItems ? this.selectListItems.get() : [];
  },
  selectListItems:[],
  current:function(data){
    if(this.currentReactive ? this.currentReactive.get() == data : false)
      return 'selected';
    else
      return '';
  },
  console:function(data){
    console.log(data);
  },
  bug:new ReactiveVar('ReactiveVar Test')
});

Template.SelectComponent.onCreated(function(){
  var self = this;
  this.autorun(function(){
      console.log(self.data.bug);
  });
});

Template.SelectComponent.onRendered(function(){
  let self = this;
  console.log('rendered select component');
  Tracker.autorun(function(){
      self.subscribeHandle = self.data.subscribe({
        onReady:function(){
        },
        onStop:function(){

        }
      });
  });

  var myApp = new Framework7();

  var $$ = Dom7;

  $$('.popup-select').on('open', function () {
    console.log('About Popup opened')
  });
  $$('.popup-select').on('close', function () {
    console.log('About Popup is closing')
  });
});
//
Template.SelectComponent.events({
});
//
Template.SelectComponent.onDestroyed(function(){
  var myApp = new Framework7();
  var $$ = Dom7;
  myApp.closeModal($$('.popup-select'));
  if(!!self.subscribeHandle)
    self.subscribeHandle.stop();
});
