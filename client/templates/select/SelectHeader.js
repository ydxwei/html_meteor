Template.SelectHeader.helpers({
  selected:function(){
    return !!this.selectedVar ? this.selectedVar.get()[this.name()].get() : '';
  },
  className:function(){
    return !!this['headerClass'] ? this.headerClass() : '';
  }
});
Template.SelectHeader.onCreated(function(){
  let select = this.data.selectedVar.get()[this.data.name()];
  if(this.data.currentReactive ? this.data.currentReactive.get() == this.data.name() : false){
      select.set('selected');
  }else {
    select.set('');
  }
});
Template.SelectHeader.onRendered(function(){
});
Template.SelectHeader.events({
  'click .head-item':function(e){
    if(this.currentReactive.get() !== this.name()){
        let select = this.selectedVar.get();
        _.each(select,function(value,index,list){
          list[index].set('');
        })
        select[this.name()].set('selected');
        this.currentReactive.set(this.name());
    }
  }
});
