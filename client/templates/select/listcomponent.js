import {EJSON} from 'meteor/ejson';
Template.ListComponent.helpers({
  name:'',
  templateInstance: null,
  selector:new ReactiveVar({}),
  // list:function(){
  //   console.log(this);
  //   return null;
  // },
  isCurrent:function(current){
    return current ? current.get() == this.name : false;
  },
  next:'',
  parent:null,
  stringify:function(data){
    return EJSON.stringify(data);
  },
  subscribeHandle:null
});

Template.ListComponent.onRendered(function(){

});

Template.ListComponent.events({
  'click li.item-content':function(e){
    var data = EJSON.parse($(e.currentTarget).attr('value'));
    var before = this.current.get();
    this.currentData.set(data);
    this.current.set(this.next);
    var current = this.current.get();
    let select = this.selectedVar.get();
    _.each(select,function(value,index,list){
        list[index].set('');
    });
    this.nextSub(data.code);
    if(typeof this.closeModule  != 'undefined'){
      this.closeModule();
    }
  }
});

Template.ListComponent.onDestroyed(function(){
  console.log(this);
});
