goodsController = BaseController.extend({
	template:'goods',
	center:'',
	left:'商品',
	mainView:'goods',
	merchant_id:0,
	waitOn:function(){
		let self = this;
		self.limit = 8;
		return self.subscribeHandleDict['goods'] = Meteor.subscribe('goods',{merchant_id:parseInt(this.params.query.merchant_id)},{limit:self.limit},function(){
			self.ready();
		});
	},
	onRendered:function(self){
		self.paging({selector:{merchant_id:parseInt(self.params.query.merchant_id)},sorts:{id:-1},perpages:10},function(options){
			self.scrolling = true;
			self.limit += options.perpages;
			Meteor.subscribe('goods',{merchant_id:parseInt(self.params.query.merchant_id)},{limit:self.limit},function(){
				self.ready();
			});
			// if(subscribe){
			// 	subscribe.stop();
			// }
		});
	},
	data:function(){
		var self = this;
		return{
			scroll:'infinite-scroll',
			goods:function(){
				return goods.find({merchant_id:parseInt(self.params.query.merchant_id)}).fetch();
			}
		}
	}
})

goodsDetailController = BaseController.extend({
	template:'goodsdetail',
	center:'商品详情',
	left:'',
	right: '<a href="/home" class="link" style="margin-right:2px;font-size:1.0rem;color:#fff">首页</a>',
	mainView:'goods-detail',
	waitOn:function(){
		let self = this;
		Tracker.autorun(function(){
			Meteor.subscribe('goods',{id:parseInt(self.params.id)},{limit:1},function(){
				self.ready();
			});
			Meteor.subscribe('merchant',{id:parseInt(self.params.query.merchant_id)},{limit:1},function(){
				self.ready();
			});
		})
	},
	onRendered:function(){
		setTimeout(function(){
		var myApp = new Framework7();
		var mySwiper = myApp.swiper('.swiper-container', {
			pagination:".swiper-pagination",
			speed: 400,
			spaceBetween: 1
		});
		},1000)
	},
	data:function(){
		var self = this;
		return{
			goods:goods.findOne({id:parseInt(self.params.id)}),
			merchant:merchant.findOne({id:parseInt(self.params.query.merchant_id)})
		}
	}
})
