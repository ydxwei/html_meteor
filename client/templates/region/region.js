RegionController = BaseController.extend({
    template:'Region',
    headerTransparent:false,
	left: '选择地址',
	mainView:'Region',
	citySubscribe:null,
	districtSubscribe:null,
	siteSubscribe:null,

	searchSite: new ReactiveVar([]),
	siteList: new ReactiveVar([]),
	waitOn: function () {
		let self = this;
		Tracker.autorun(function(){
			return [
				Meteor.subscribe('provinces',{},{sort:{first_word:1}}),
				Meteor.subscribe('sites',{state:'enable'},{},self.subscribeCallbacks())
			];
		});
	},
	onRendered:function (controller) {
		let self = this;
		this.siteGeo();

		$(".f7-region-content-top span").click(function(evt){
			var index = $(".f7-region-content-top span").index(this);
			self.selected(index, index);
		});

		$('.f7-search-content').hide();
		self.selected(0, 0);

		$('#search').bind('input propertychange', function() {
			let search = $(this).val();
			console.log(search)
			if (search) {
				$('.f7-region').hide();
				$('.f7-search-content').show();
				let s = Sites.find({name: {$regex: search, $options: 'i'}}).fetch();
				self.searchSite.set(s);
			}else{
				$('.f7-region').show();
				$('.f7-search-content').hide();
			}
		});
	},
	data:function () {
		let self = this;
		return{
			siteName: localStorage.siteName,
			hotSite: function (params) {
				return Sites.find({hot_site: 'yes'},{limit:6}).fetch();
			},
			lnglatSite: function (params) {
				let slng = 117.242346;
				let slat = 31.785381;
				if (localStorage.lat || localStorage.lng) {
					let slat = localStorage.lat;
					let slng = localStorage.lng;
				}
				return Sites.find({ location: { $near: { $geometry: { type: 'Point', coordinates: [slng,slat] } } } },{limit:6}).fetch();
			},
			searchSite: function (params) {
				return self.searchSite.get();
			},
			provinceList: Provinces.find().fetch(),
			cityList: function (params) {
				return Cities.find().fetch();
			},
			districtList: function (params) {
				return Districts.find().fetch();
			},
			siteList: function (params) {
				return self.siteList.get();
			}
		}
	},
	events:function(controller){
		let self = this;
		return {
			'click #province label':function(e){
				$(this).find("input[name='province']").attr("checked", 'checked');
				let p_code = e.target.id;
				let name = e.target.innerText;
					name = name.replace('check','');
				console.log(p_code)
				if (p_code && name) {
					self.selected(0,1, name);
					self.subscribeHandle = function(){
						return self.citySubscribe = Meteor.subscribe('cities',{province_code:parseInt(p_code)},{},self.subscribeCallbacks());
					}
					if(self.citySubscribe){
						self.citySubscribe.stop();
					}else{
						self.citySubscribe = Meteor.subscribe('cities',{province_code:parseInt(p_code)},{},self.subscribeCallbacks());
					}
				}
			},
			'click #city label':function(e){
				$(this).find("input[name='city']").attr("checked", 'checked');
				let c_code = e.target.id;
				let name = e.target.innerText;
					name = name.replace('check','');
				if (c_code && name) {
					self.selected(1,2, name);
					self.subscribeHandle = function(){
						return self.districtSubscribe = Meteor.subscribe('districts',{city_code:parseInt(c_code)},{},self.subscribeCallbacks());
					}
					if(self.districtSubscribe){
						self.districtSubscribe.stop();
					}else{
						self.districtSubscribe = Meteor.subscribe('districts',{city_code:parseInt(c_code)},{},self.subscribeCallbacks());
					}
				}
			},
			'click #district label':function(e){
				$(this).find("input[name='district']").attr("checked", 'checked');
				let d_code = e.target.id;
				let name = e.target.innerText;
					name = name.replace('check','');

					console.log(d_code)
				if (d_code && name) {
					self.selected(2,3, name);

					let s = Sites.find({district_code:parseInt(d_code), state:'enable'}).fetch();
					self.siteList.set(s);
				}
			},
			'click #site label':function(e){
				$(this).find("input[name='site']").attr("checked", 'checked');
				let s_code = e.target.id;
				let name = e.target.innerText;
				name = name.replace('check','');
				if (s_code && name) {
					self.selected(3,4, name);
					localStorage.siteId = s_code;
					localStorage.siteName = name;

					window.location.href = '/home';
				}
			},
			'click #search-region label':function(e){
				$(this).find("input[name='site']").attr("checked", 'checked');
				let s_code = e.target.id;
				let name = e.target.innerText;
				name = name.replace('check','');
				if (s_code && name) {
					localStorage.siteId = s_code;
					localStorage.siteName = name;

					window.location.href = '/home';
				}
			},
			'click .site_click':function(e){
				let s_code = e.target.id;
				let name = e.target.innerText;
				name = name.replace('check','');
				if (s_code && name) {
					localStorage.siteId = s_code;
					localStorage.siteName = name;

					window.location.href = '/home';
				}
			},
		}
	},
	selected: function (id, i, params=null){
		$(".f7-region-list-block ul").hide().eq(i).show();
		if (params != null && params != ''){
			$(".f7-region-content-top span:eq("+id +")").html(params);

			// for (var k = 1; k < 5; k++) {
			// 	if (k >= i) {
			// 		$(".f7-region-content-top span:eq("+k +")").html('');
			// 	}
			// }
		}
		$(".f7-region-content-top span").removeClass('f7-region-radio').eq(i).addClass('f7-region-radio');
	},
})
