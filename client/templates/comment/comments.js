commentController = BaseController.extend({
	template:'comment',
	center:'评论',
	mainView: 'comment-view',
	onRendered:function(){
		$('.list-block').on('click','.x7-comment-button',function(){
			$(this).parent().parent().parent().remove();
		})
	},
	waitOn:function(){
		let self = this;
		Tracker.autorun(function(){
			self.user = self.currentUser();
			self.user_id = self.user.id;
			if(self.params.query.merchant_id){
				self.comselector = {merchant_id:parseInt(self.params.query.merchant_id)}
			}else{
				self.comselector = {user_id:self.user_id}
			}
			Meteor.subscribe('comments',self.comselector,{limit:20});
		})
	},
	data: function(){
		return {
			merchantComment:function(){
				return Comments.find().fetch();
			},
		};
	},
});
