OrderCommentsController = BaseController.extend({
  template:'orderComment',
  headerTransparent:false,
  left: '评价',
  headerBack: '/order',
  onRendered:function(self){
    var starNum= 0;
    $(".f7-evaluate-content-top i").on('click',function(e){
      starNum = e.target.id;
      if (starNum > 0 && starNum < 6){
        for  (var i=1; i<=starNum; i++){
          $("#"+i).html("star_fill");
        }
        var j = parseInt(starNum) + parseInt(1);
        for (; j<=5; j++){
          $("#"+j).html("star");
        }
      }
    });
		var sub = true;
    $("#submitComment").on('click', function(){
			if(sub == false)return;
      var inputText = $("#commentText").val();
      var userId = self.params.query.user_id;
      var post_data = {
        stars:starNum,
        context:inputText,
        merchant_id:$('#merchant_id').val(),
        order_id:parseInt(self.params.query.order_id),
        user_id:userId
      }
      self.post('user/comment',post_data).then(function(res){
				sub = false;
        Router.go('/order')
      },function(){

      })
    });
  },
  waitOn:function(){
    Meteor.subscribe('orders',{id:parseInt(this.params.query.order_id)},{},function(){
      
    });
  },
  data: function(){
    return {
      orderInfo:function(){
        return Orders.findOne();
      }
    };
  },
});
