BaseController = RouteController.extend({
	layoutTemplate: "Layout",
	loadingTemplate: 'Loading',
	yieldRegions: {
		'Header': { to: 'header' }
	},
	apiServer: __meteor_runtime_config__.API_HOST,
	//apiServer: 'http://api.37shenghuo.com',
	apiVersion: 'v2',
	mainView: '',
	center: '',
	left: '',
	right: '',
	transparent: false,
	eventMap: {},
	app: null,
	$$: '',
	apiKey: '1fafd881b4e1948d0df79fcaf7069202',
	apiSecret: 'c91a9574c64269c09df5f781f5e67aa6e91a66e2f67b44986820b32b55560401',
	subscribeHandleDict: {},
	subscribeHandle: null,
	curUser: {},
	curUserSub: null,
	currentUserData: new ReactiveVar(null),
	userBaseId:new ReactiveVar(),
	subscribeCallbacks: function () {
		let self = this;
		return {
			onReady: function () {
				self.onReadyCallBack();
				return self.ready();
			},
			onStop: function () {
				self.onStopCallBack();
				self.subscribeHandle();
			}
		}
	},
	onReadyCallBack: function () {

	},
	onStopCallBack: function () {

	},
	subscribeHandle: function () {

	},
	events: function () {
		return {};
	},
	accessToken: function () {
    if(typeof(this.accessTokensSub)=='undefined'){
      this.accessTokensSub = Meteor.subscribe('accessTokens', {}, function () {
  			//this.stop();
  		});
    }
	},
	on: function (event, callback) {
		let self = this;
		if (!self.eventMap[event]) {
			self.eventMap[event] = {};
		}
		if (!self.eventMap[event]['callbacks']) {
			self.eventMap[event]['callbacks'] = {};
		}
		self.eventMap[event]['callbacks'][CryptoJS.MD5(callback.toString()).toString()] = callback;
		$(document).on(event, function (evt) {
			$.each(self.eventMap[event]['callbacks'], function (idx, func) {
				func({
					event: evt,
					data: self.eventMap[event]['data']
				});
			});
		});
	},
	off: function (event, callback) {
		if (arguments.length == 2) {
			let key = CryptoJS.MD5(callback.toString()).toString();
			delete self.eventMap[event]['callbacks'][key];
		} else {
			delete self.eventMap[event];
		}
	},
	emit: function (event, params) {
		if (!this.eventMap[event]) {
			this.eventMap[event] = {};
		}
		this.eventMap[event]['data'] = params;
		$(document).trigger(event);
	},
	routeParam: function (key) {
		return this.params[key];
	},
	query: function (key) {
		return this.params.query[key];
	},
	platform: function () {
		return window.platform();
	},
	currentUser: function () {
		var user = Users.findOne();
		return user;
	},
	setUser:function(user){
		console.log(user);
		if(user){
			console.log('user set to ReactiveVar');
			this.curUser = user;
			localStorage.curUser = JSON.stringify(this.curUser);
			this.currentUserData.set(this.curUser);
		}
	},
	isLogined() {
		return (typeof (localStorage.tokenData) != 'undefined' && localStorage.tokenData != null &&
			localStorage.tokenData != 'null') || (typeof (this.params.query['token']) != 'undefined' &&
				this.params.query['token'] != null);
	},
	showBottom:function(){
		var user = this.currentUser();
		console.log('current user info exist ',!!user);
		return !!user;
	},
	getCurrentUser(token) {
		let self = this;
		if (typeof (localStorage.curUser) != 'undefined' && localStorage.curUser != 'undefined' && localStorage.curUser != null && localStorage.curUser != 'null') {
			console.log('local user info');
			let data = JSON.parse(localStorage.curUser);
			self.curUser = data;
			self.currentUserData.set(self.curUser);
		}
		self.userBaseId.set(token['user_base_id']);
	},
	curUserSub:function(id){
		var self = this;
		Meteor.subscribe('currentUser', id, {onReady:function () {
			var user = Users.findOne();
			console.log(user);
		},onStop:function(){}});
	},
	userDataCallBack:function(id){
		var self = this;
		Meteor.call('currentUser', id, function (error, result) {
			if (!error) {
				if (result.status == 'OK') {
					self.curUser = result.user;
					localStorage.curUser = JSON.stringify(self.curUser);
					self.currentUserData.set(self.curUser);
				}
				console.log(result);
			}else{
				console.log(error);
			}
		});
	},
	now:function () {
		return (new Date()).getTime();
	},
	isWeiXin:function(){
			var ua = window.navigator.userAgent.toLowerCase();
			if(ua.match(/MicroMessenger/i) == 'micromessenger'){
			    return true;
			}else{
			    return false;
			}
	},
	getTokenData:function(){
		return (typeof localStorage.tokenData != 'undefined' && localStorage.tokenData != null) ?
		JSON.parse(localStorage.tokenData) : null;
	},
	loginToken: function () {
		if (!this.isLogined())
			return;
		let self = this;
		let now = this.now() / 1000;
		let token = this.getTokenData();
		if(token){
			if(token.ttl > now){
				this.getCurrentUser(token);
			}else if(token.ttl < now && token.refresh_ttl > now){
				this.refreshToken();
			}

		}else{
			token = this.query('token');
			this.subToken(token,now);
		}
	},
	refreshToken:function(){
		var self = this;
		this.get('auth/token/refresh').then(function(response){
			if(response.data.state == "OK"){
				if(response.data.status_code == ResponseDict.AUTH_TOKEN_BUILDER_OK){
					LoginToken.insert(ResponseDict.data.data.token);
					localStorage.tokenData = JSON.stringify(ResponseDict.data.data.token);
					self.router.go('home');
				}
			}else {
				self.app.alert(response.data.message);
			}
		},function(error){

		});
	},
	subToken:function (token,now) {
		let selector = {
			'$and': [
				{
					'refresh_ttl': {
						'$gt': now
					}
				}, {
					'$or': [{
						token: token
					}, {
						md5: token
					}]
				}
			]
		};
		var self = this;
		Meteor.subscribe('loginTokens', selector, { limit: 1 }, function () {
			if (this.ready) {
				let token = LoginToken.findOne();
				if (!token) {
					delete localStorage.tokenData;
					delete localStorage.curUser;
				} else {
					localStorage.tokenData = JSON.stringify(token);
					if(token.ttl > now){
						self.getCurrentUser(token);
					}else{
						self.refreshToken();
					}
				}
			}
		});
	},
	getToken() {
		var token = this.getTokenData();
		return  token ? token.token : null;
	},
	buildQuery: function (query, key) {
		let self = this;
		let queryStr = ''
		if (_.isString(query) || _.isNumber(query) || _.isNull(query)) {
			queryStr = !!key ? (key + '=' + query) : query;
		} else {
			if (_.isObject(query)) {
				_.each(query, function (val, index) {
					let tmp = '';
					if (key) {
						tmp += '[' + index + ']';
					} else {
						tmp = index;
					}
					queryStr += '&' + self.buildQuery(val, tmp);
				});
			} else {
				queryStr = '';
			}
		}
		return queryStr;
	},
	get: function (url, query) {
		let self = this;
		query = self.buildQuery(query, '');
		if (query) {
			query = '?' + query.substr(1);
		}
		return new Promise(function (resolve, reject) {
			HTTP.get(self.apiServer + "/" + self.apiVersion + "/" + url + query,{
				headers: {
					'authorization': 'bearer ' + self.getToken()
				}
			}, function (error, result) {
				if (error) {
					reject(error);
				} else {
					resolve(result);
				}
			});
		});
	},
	post: function (url, data) {
		let self = this;
		return new Promise(function (resolve, reject) {
			HTTP.post(self.apiServer + "/" + self.apiVersion + "/" + url, {
				data: data,
				headers: {
					'authorization': 'bearer ' + self.getToken()
				}
			}, function (error, result) {
				if (error) {
					reject(error);
				} else {
					resolve(result);
				}
			});
		});
	},
	put: function (url, data, callback) {
		let self = this;
		return new Promise(function (resolve, reject) {
			HTTP.put(self.apiServer + "/" + self.apiVersion + "/" + url, {
				data: data,
				headers: {
					'authorization': 'bearer ' + self.getToken()
				}
			}, function (error, result) {
				if (error) {
					reject(error);
				} else {
					resolve(result);
				}
			});
		});
	},
	delete: function (id, callback) {
		let self = this;
		return new Promise(function (resolve, reject) {
			HTTP.del(self.apiServer + "/" + self.apiVersion + "/" + url, {
				headers: {
					'authorization': 'bearer ' + self.getToken()
				}
			}, function (error, result) {
				if (error) {
					reject(error);
				} else {
					resolve(result);
				}
			});
		});
	},
	onBeforeAction: function () {
		if (!window.app) {
			window.app = new Framework7({
				pushState: true,
				swipePanel: 'left',
				modalTitle: "提示",
				modalButtonOk: "确定",
				modalButtonCancel: "取消",
			});
		}
		this.app = window.app;
		if (!window.$$) {
			window.$$ = Dom7;
		}
		this.$$ = window.$$;
		Template[this.template].onRendered(this.renderCallback);
		//Template[this.template].onDestroyed(this.distroyCallback);
		Template[this.template].events(this.events(this));
		let next = this.beforeAction();
		return !!next ? next() : '';
	},
	distroyCallback: function () {
		let controller = Iron.controller();
		controller.onDestroyed(controller);
	},
	onDestroyed: function () {
	},
	onStop: function () {
		this.subscribeHandle = function () {

		}
		_.each(this.subscribeHandleDict, function (subscribe) {
			subscribe.stop();
		});
		this.subscribeHandleDict = {};
		this.routeStop();
	},
	routeStop: function () {

	},
	beforeAction: function () {
		return this.next;
	},
	renderCallback: function () {
		let controller = Iron.controller();
		if (Template.Header.url) Template.Header.url.set(controller.headerBack);
		if (Template.Header.center) Template.Header.center.set(Spacebars.SafeString(controller.center));
		if (Template.Header.left) Template.Header.left.set(Spacebars.SafeString(controller.left));
		if (Template.Header.right) Template.Header.right.set(Spacebars.SafeString(controller.right));
		let transparent = !!controller.headerTransparent ? 'transparent mask' : '';
		if (Template.Header.transparent) Template.Header.transparent.set(transparent);
		$('#views').removeAttr('class');
		var myApp = new Framework7;
		var $$ = Dom7;
		myApp.detachInfiniteScroll($$('.infinite-scroll'));
		$('#views').addClass("views " + controller.platform() + " " + controller.mainView);
		controller.onRendered(controller);
	},
	onRendered: function (self) {
	},
	waitOn: function () {
		let meteor = '';
		var self = this;
		Tracker.autorun(function(){
			meteor = self.accessToken();
			meteor = self.loginToken();
			console.log('sub user 0');
			console.log(self.userBaseId.get());
			self.curUserSub(self.userBaseId.get());
			self.setUser(self.currentUser());
			//self.userDataCallBack(self.userBaseId.get());
		});

		self.siteGeo();
		$('.modal-in,.modal-overlay').remove();
		return ;
	},
	wait: function () {
	},
	paging: function (options, callback) {
		var myApp = new Framework7;
		var $$ = Dom7;
		var self = this;
		if (!this.scrollData)
			this.scrollData = {
				perpages: 4,
				maxItems: 60,
				sort: { id: -1 },
				selector: {},
				curlistLenght: 0,
				lastIndex: 0,
				scrolling: false,
				end: false
			};
		this.scrollData = _.extend(this.scrollData, options);
		$$('.infinite-scroll').on('infinite', function () {
			if (!$('.page-content').hasClass('infinite-scroll'))
				return;
			var listLenght = $$('.list-block li').length;
			if (self.scrollData.end) {
				return;
			}
			if (listLenght > self.scrollData.curlistLenght) {
				self.scrollData.curlistLenght = listLenght;
			}
			if (!self.scrolling) {
				self.scrollData.scrolling = true;
				self.scrollData.lastIndex += self.scrollData.perpages;
				if (callback) callback(self.scrollData);
			}
		});
	},
	gtlng: function (lat1, lng1, lat2, lng2) {
		var EARTH_RADIUS = 6378137.0;    //单位M
		var PI = Math.PI;
		var fa = (lat1 + lat2) / 2;
		var ga = (lat1 - lat2) / 2;
		var la = (lng1 - lng2) / 2;
		var f = fa * PI / 180.0;
		var g = ga * PI / 180.0;
		var l = la * PI / 180.0;
		var sg = Math.sin(g);
		var sl = Math.sin(l);
		var sf = Math.sin(f);
		var s, c, w, r, d, h1, h2;
		var a = EARTH_RADIUS;
		var fl = 1 / 298.257;
		sg = sg * sg;
		sl = sl * sl;
		sf = sf * sf;
		s = sg * (1 - sl) + (1 - sf) * sl;
		c = (1 - sg) * (1 - sl) + sf * sl;
		w = Math.atan(Math.sqrt(s / c));
		r = Math.sqrt(s * c) / w;
		d = 2 * w * a;
		h1 = (3 * r - 1) / 2 / c;
		h2 = (3 * r + 1) / 2 / s;
		return d * (1 + fl * (h1 * sf * (1 - sg) - h2 * (1 - sf) * sg));
	},
	lnglat: new ReactiveVar([117.242346, 31.785381]),
	currentSite: new ReactiveVar(null),
	siteGeo: function (url) {
		let self = this;
		var url = "user/wxconfig";
		let data = {url:location.href.split("#")[0]};
		self.get(url, data).then(function (res){
			if(res.data){
				wx.config({
					debug: false,
					appId: res.data.appId,
					timestamp: res.data.timestamp,
					nonceStr: res.data.nonceStr,
					signature: res.data.signature,
					jsApiList: ['getLocation'] // 必填，需要使用的JS接口列表，所有JS接口列表见附录2
				});
				wx.ready(function(){
					wx.getLocation({
						type: 'wgs84',
						success: function (res) {
							var lat = res.latitude; // 纬度，浮点数，范围为90 ~ -90
							var lng = res.longitude; // 经度，浮点数，范围为180 ~ -180。
							localStorage.lat = lat;
							localStorage.lng = lng;
							//alert('当前：经度，'+lng+';纬度，'+lat);
							console.log('当前：经度，'+lng+';纬度，'+lat);
							if(lat && lng){
								self.lnglat.set([parseFloat(lng), parseFloat(lat)]);
								//console.log(self.lnglat.get());
								Meteor.subscribe('sites', { location: { $near: { $geometry: { type: 'Point', coordinates: self.lnglat.get() } } } }, { limit: 1 }, {onReady:function () {
									console.log(111);
									let site = Sites.findOne();
									console.log(site);
									if (site) {
										localStorage.siteId = site.id;
										localStorage.siteName = site.name;
										//alert(JSON.stringify(site));
										self.currentSite.set(site);
									}
									
								},onStop:function(){}});
							}
						},
						cancel: function (res) {
							// alert('用户拒绝授权获取地理位置');
							console.log('用户拒绝授权获取地理位置');
						},
						fail: function (res) {
							// self.app.showPreloader('定位失败！')
							// setTimeout(function () {
							// 	self.app.hidePreloader();
							// }, 2000);
							// alert(JSON.stringify(res));
							//alert('定位失败！');
							console.log('定位失败！');
						}
					});
				})
			}
		}, function (errer){
			console.log('获取微信配置参数失败');
		});
	}
});
