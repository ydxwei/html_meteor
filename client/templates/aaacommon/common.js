function platform(){
  if(/android/i.test(navigator.userAgent)){
    return 'android';//这是Android平台下浏览器
  }
  if(/(iPhone|iPad|iPod|iOS)/i.test(navigator.userAgent)){
    return 'iphone';//这是iOS平台下浏览器
  }
  if(/Linux/i.test(navigator.userAgent)){
    return 'linux browser';//这是Linux平台下浏览器
  }
  if(/Linux/i.test(navigator.platform)){
    return 'linux os';//这是Linux操作系统平台
  }
  if(/MicroMessenger/i.test(navigator.userAgent)){
     return 'wechat browser';//这是微信平台下浏览器
  }

  return 'iphone';
}
this.platform = platform;
console.log(this.platform);
