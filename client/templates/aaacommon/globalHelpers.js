Template.registerHelper('eq', (para1, para2) => {
	return para1 == para2;
});

Template.registerHelper('dis', (para) => {
	var a = para*10;
	return a.toFixed(2);
});

Template.registerHelper('paytype', (para) => {
	if(para == 'alipay')return '支付宝';
	if(para == 'wxpay')return '微信';
	return '其他';
});
Template.registerHelper('timetodate', (para) => {
	if(!para || para == '')return '';
	return new Date(parseInt(para) * 1000).toLocaleString().substr(0,17);
});
Template.registerHelper('coutype', (para) => {
	if(para == 'cash'){
		return '现金券';
	}
	if(para == 'discount'){
		return '限时折扣';
	}
	if(para == 'full_cut'){
		return '限时满减';
	}
});

Template.registerHelper('coumoney', (para1,para2) => {
	if(para1 == 'cash' || para1 == 'full_cut'){
		return '￥'+para2;
	}
	if(para1 == 'discount'){
		return para2 + '折';
	}
});
Template.registerHelper('days', (para) => {
	if(para == 0){
		return '今日';
	}
	if(para == 1){
		return '明日';
	}
	return '';
});
Template.registerHelper('fulls', (para1,para2) => {
	if(para1 == 'full_cut'){
		return '满'+para2+'减';
	}
	return '';
});

Template.registerHelper('couponDetail', (para) => {
	if(para['cash']){
		return '￥' + para['cash'] + '元';
	}
	if(para['discount']){
		return para['discount']*10 + '折';
	}
	if(para['cut']){
		return '￥' + para['cut'] + '元';
	}
	return ;
});
Template.registerHelper('getLocalTime', (para) => {
	var time = new Date(para*1000);
	var y = time.getFullYear();
	var m = time.getMonth()+1;
	var d = time.getDate();
	// var h = time.getHours();
	// var mm = time.getMinutes();
	// var s = time.getSeconds();
	return y+'年'+m+'月'+d+'日';
});

Template.registerHelper('getParam', (para) => {
	var url = window.document.location.href.toString();
	var u = url.split("?");
	if(typeof(u[1]) == "string"){
			u = u[1].split("&");
			var get = {};
			for(var i in u){
					var j = u[i].split("=");
					get[j[0]] = j[1];
			}
			return get[para];
	} else {
			return {};
	}

});

Template.registerHelper('orderState', (para) => {
	if(para == 'nopay'){
		return '未付款';
	}else if(para == 'pay'){
		return '已付款';
	}else if(para == 'cancel'){
		return '订单已取消';
	}else if(para == 'refunding'){
		return '退款中';
	}else if(para == 'apply'){
		return '申请退款中';
	}else if(para == 'refunded'){
		return '已退款';
	}else if(para == 'refuse'){
		return '退款已拒绝';
	}
	return ;
});

Template.registerHelper('strtohtml', (para) => {
	if(para != '' && para != null){
			return Spacebars.SafeString(para);
	}
	return '';
});

Template.registerHelper('ispay', (para) => {
	if(para == 'nopay' || para == 'cancel'){
			return false;
	}
	return true;
});

Template.registerHelper('gopay', (para) => {
	return para == 'nopay';
});

Template.registerHelper('ordertype', (para) => {
	return para == 'goods';
});

Template.registerHelper('gorefund', (para) => {
	return para == 'pay';
});

Template.registerHelper('strlen', (para) => {
	if(!para)return false;
	if(para.length > 0){
		return true;
	}
	return false;
});

Template.registerHelper('weeks', (para) => {
	let weeks = ['星期日', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六'];
	return weeks[para];
});
