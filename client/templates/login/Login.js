LoginController = BaseController.extend({
  template:'Login',
  mainView:'login-view login-screen',
  mobile:'',
  code:'',
  newUser:true,
  waitOn:function(){
    // this.accessToken();
    // return this.loginToken();
  },
  appInit:function(){

  },
  login:function(){

  },
  onRendered:function(controller){
    this.siteGeo()
      controller.app.loginScreen();
      $('.login-view .mobile-input').bind('input propertychange',function(e){
        let val = $(e.target).val();
        if(val.length >= 11){
           controller.mobile = val;
           $('.login-view .mobile-code a').removeClass('disabled');
           $('.login-view .mobile-code a').removeAttr('disabled');
        }else{
          controller.mobile = '';
          $('.login-view .mobile-code a').addClass('disabled');
          $('.login-view .mobile-code a').attr('disabled','disabled');
          $('.login-view .mobile-code input').attr('readonly','readonly');
          $('.login-view .login-button').attr('disabled','disabled');
        }
      });

      $('.login-view .mobile-code').bind('input propertychange',function(e){
        let val = $(e.target).val();
        if(val.length >= 6){
          controller.code = val;
           $('.login-view .login-button').removeAttr('disabled');
           //$('.login-view .mobile-code a').removeAttr('disabled');
        }else{
          controller.code = '';
          $('.login-view .login-button').attr('disabled','disabled');
          //$('.login-view .mobile-code a').attr('disabled','disabled');
        }
      });
  },
  beforeAction:function(){
    if(this.isLogined()){
      if(!this.currentUser['province_code']||!this.currentUser['city_code']||
      !this.currentUser['district_code']||!this.currentUser['university_code']||
      !this.currentUser['mark_info']){
        this.router.go('newuser.info');
      }else{
        this.router.go('home',{hash:"body"});
      }

      return null;
    }
    return this.next();
  },
  events:function(controller){
      var  self = this;
      return {
        'click .next':function(){
          return location.href = location.protocol+'//'+location.hostname+Router.path('home');
        },
        'click .mobile-code #_get_mobile_code':function(e){
          e.stopPropagation();
          $(e.currentTarget).attr('disabled','disabled');
          let timeout = 59;
          let mobile = $('.login-view .mobile-input input').val();
          controller.get('user/mobile-code/'+mobile).then(function(res){
            if(res.data.state == "OK"){
               $('.login-view .mobile-code input').removeAttr('readonly');
               if(res.data.status_code == ResponseDict.USER_NOT_EXIST){
                 controller.newUser = true;
               }else if(res.data.status_code == ResponseDict.USER_EXIST){
                 controller.newUser = false;
               }
            }
          },function(error){

          });
          let id = setInterval(function(){
            $(e.currentTarget).html(timeout+'秒');
            if(--timeout){

            }else{
              $(e.currentTarget).removeAttr('disabled');
              $(e.currentTarget).html('获取验证码');
              clearInterval(id);
            }
          },1000);
          return false;
        },
        'click .login-button':function(e){
          e.stopPropagation();
          if( self.newUser ){
            let thirdId = self.query('third_party_id');
            self.post('user/sign-in',{mobile:self.mobile,code:self.code,third_party_id:thirdId}).then(function(res){
              if(res.data.state == "OK"){
                var token =  res.data.data.token;
                //self.userBaseId.set(res.data.data.user_base_id);
                //localStorage.userBaseId = res.data.data.user_base_id;
                LoginToken.insert(token);
                localStorage.tokenData = JSON.stringify(res.data.data.token);
                return location.href = location.protocol+'//'+location.hostname+Router.path('newuser.info');
              }else{
                self.app.alert(res.data.error);
                return false;
              }
            },function(error){
              console.log(error);
            });
          }else{
            self.post('user/sign-up',{mobile:self.mobile,code:self.code}).then(function(res){
              if(res.data.state == "OK"){
                if(res.data.status_code == ResponseDict.AUTH_TOKEN_BUILDER_OK){
                  //self.userBaseId.set(res.data.data.user_base_id);
                  //localStorage.userBaseId = res.data.data.user_base_id;
                  LoginToken.insert(res.data.data.token);
                  localStorage.tokenData = JSON.stringify(res.data.data.token);
                  return location.href = location.protocol+'//'+location.hostname+Router.path('home');
                }
              }else {
                self.app.alert(res.data.error);
                return false;
              }
            },function(error){
              console.log(error);
            });
          }
          return false;
        },
        'click .remember-me .check-box':function(e){
          if($(e.currentTarget).attr('checked') =="checked"){
            $(e.currentTarget).find('i').removeClass('hidden');
            $(e.currentTarget).removeAttr('checked');
          }else {
            $(e.currentTarget).find('i').addClass('hidden');
            $(e.currentTarget).attr('checked','checked');
          }
        }
      };
  },
  data:function(){
    return {};
  }
});
