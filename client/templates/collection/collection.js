CollectionController = BaseController.extend({
    template:'collection',
    headerTransparent:false,
    center:'我的收藏',
	waitOn: function (controller) {
		this.limit = 20;
    let self = this;
		Tracker.autorun(function(){
			self.user = self.currentUser();
			self.user_id = self.user.id;
			Meteor.subscribe('collections',{user_id:self.user_id}, {sort:{id:1},limit:self.limit});
		})
	},
    onRendered:function (controller) {
		    controller.paging('collections',{},{},this.limit);
    },
    data:function () {
		let self = this;
		return {
			scroll:'infinite-scroll',
			Collections:Collections.find().fetch(),
		}
	},
	events: function (controller) {
		let self = this;
		return {
			'click .f7-collection-button': function (e) {
				let id = e.target.id;
				if(id){
					//controller.app.confirm('删除收藏?', [], function () {
						controller.delete('user/collection/' + id).then(function (res) {

						},function (errer) {
						})
					//});
				}
			}
		}
	}
})
