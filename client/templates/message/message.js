messageController = BaseController.extend({
	template:'message',
	center:'消息',
	mainView: 'message-view',
	onRendered:function(){
		$("#messageId").on('click',function(){
			//$(this).hide();
		});
	},
	waitOn:function(){
		let self = this;
		Meteor.subscribe('messages',function(){
			return self.ready();
		});
	},
	data: function(){
		return {
			messagesList:function(){
				return Messages.find().fetch();
			},
		};
	},
	
})

messageDetailController = BaseController.extend({
	template:'messageDetail',
	center:'消息详情',
	mainView: 'messageDetail-view',
	waitOn:function(){
		let self = this;
		Tracker.autorun(function(){
			return Meteor.subscribe('messages',{id:2},{});
		});	
	},
	data: function(){
		return {
			messagesInfo:function(){
				return Messages.findOne();
			},
		};
	},
})
