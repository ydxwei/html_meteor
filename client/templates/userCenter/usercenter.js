UserCenterController = BaseController.extend({
  template:'UserCenter',
  center:'用户中心',
  left:'<div></div>',
  headerTransparent:false,
  mainView:'user-center-view',
  //bottomShow:true,
  bottomShow: new ReactiveVar(false),
  waitOn: function () {
    var self = this;
    Tracker.autorun(function(){
      self.bottomShow.set(self.showBottom());
    });
   },
   onRendered:function(){
     let avatar = $(".user-profile.list-block .list-item .item-content .item-media .avatar");
     let  src = avatar.attr('src');
     let  image = new Image();
     image.src = src;
     image.onload = function(){
       let rate = image.width/image.height;
       if(rate>1){
         avatar.attr('style','height:100%;width:auto;');
       }else{
           avatar.attr('style','width:100%;height:auto;');
       }
     };
   },
  data:function(){
    let self = this;
     return {
      user:function(){
 				return self.currentUser();
 			},
 			username:function(user){
        if(user != null && typeof(user) != 'undefined')
 				   return user.nick_name ? user.nick_name :(user.user_name ? user.user_name : user.mobile);
 				return '';
 			},
 			avatar:function(avatar){
 				if(avatar != null && typeof(avatar) != 'undefined'){
 					return avatar.path;
 				}else{
 					return '';
 				}
 			},
     };
  },
  action: function () {
    this.render();
  }
});
