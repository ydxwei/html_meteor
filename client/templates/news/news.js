NewsController = BaseController.extend({
    template:'News',
    headerTransparent:false, 
    left: '37头条',
    waitOn: function () {
		let self = this;
		Tracker.autorun(function(){
			return Meteor.subscribe('news',{},{sort:{id:-1},limit:2});
		});
    },
	onRendered:function () {

    },
    data:function () {
        return{
            News:function () {
                return News.find().fetch();
            }
        }
    }
});

NewsDetailController = BaseController.extend({
    template:'NewsDetail',
    headerTransparent:false,
    center: '37头条',
    waitOn: function () {
        let self = this;
        console.log(this.params)
		Tracker.autorun(function(){
			return Meteor.subscribe('news',{id:1},{});
		});
    },
	onRendered:function () {
        
    },
    data:function () {
        return{
            news:function () {
                return News.findOne();
            }
        }
    }
})