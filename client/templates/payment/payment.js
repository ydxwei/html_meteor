paymentController = BaseController.extend({
	template:'payment',
	center:'支付',
	mainView: 'payment-view',
	gopay_url:new ReactiveVar('#'),
	onRendered:function(self){
		var payType = 'wxpay';
		var myApp = new Framework7();
		self.getPayUrl(payType);
		$('.wx').on('click',function(){
			payType = 'wxpay';
			self.getPayUrl('wxpay',false);
		})
		$('.ali').on('click',function(){
			payType = 'alipay';
			$('#pay_now').attr('href','#');
			self.getPayUrl('alipay',false);
		})
		$('.pay_now').bind('click',function(){
			self.getPayUrl(payType,true);
		})
	},
	getPayUrl:function(payType,go){
		console.log(payType)
		this.post('user/pay',{id:this.params.query.order_id,pay_type:payType}).then(function(res){
			let data = res.data.data;
			console.log(data)
			if(data.errcode == 0 || data.errcode == 8045){
				if(payType == 'alipay'){
					if(go){
						location.href = '/paynotice?goto='+data.data.jsapi_pay_url;
					}else{
						$('#pay_now').attr('href','#');
					}
				}else{
						$('#pay_now').attr('href',data.data.jsapi_pay_url);
				}
			}
		},function(err){
			console.log('ddd')
		})
	},
	data: function(){
		let self = this;
		return {
			platform(os){
				return	self.platform() == os;
			},
			paymentInfo(){
				return {
					list_image: decodeURIComponent(self.params.query.list_image),
					pay_fee: self.params.query.pay_fee,
					order_no:  self.params.query.order_no,
					mname:self.params.query.mname
				}
			}
		};
	},
})


paynoticeController = BaseController.extend({
	template:'paynotice',
	center:'支付',
	mainView: 'paynoticeview',
	onRendered:function(self){

	}
})
