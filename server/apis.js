import {EJSON} from 'meteor/ejson';
Router.route('user/:id?',{where:'server'})
.post(function(){
  let data = this.request.body;
  let id   = this.params.id;
  data['id'] = parseInt(data['id']);
  let result = Users.upsert({id:parseInt(id)},data,function(){

  });
  if(result)
    this.response.end('ok\n');
  else
    this.response.end('faid\n');
});

Router.route('login/token/:id?',{where:'server'})
.post(function(){
  let data = this.request.body;
  let id   = this.params.id;
  data['id'] = parseInt(data['id']);
  let result = LoginToken.upsert({id:parseInt(id)},data,function(){

  });
  if(result)
    this.response.end('ok\n');
  else
    this.response.end('faid\n');
});

Router.route('merchant/:id?',{where:'server'})
.post(function(){
  let data = this.request.body;
  let id   = this.params.id;
  data['id'] = parseInt(data['id']);
  let result = merchant.upsert({id:parseInt(id)},data,function(){

  });
  if(result)
    this.response.end('ok\n');
  else
    this.response.end('faid\n');
});

Router.route('goods/:id?',{where:'server'})
.post(function(){
  let data = this.request.body;
  let id   = this.params.id;
  let result = goods.upsert({id:parseInt(id)},data,function(){

  });
  if(result)
    this.response.end('ok\n');
  else
    this.response.end('faid\n');
});

Router.route('order/:id?',{where:'server'})
.post(function(){
  let data = this.request.body;
  let id   = this.params.id;
  let result = Orders.upsert({id:parseInt(id)},data,function(){

  });
  if(result)
    this.response.end('ok\n');
  else
    this.response.end('faid\n');
});

Router.route('comment/:id?',{where:'server'})
.post(function(){
  let data = this.request.body;
  let id   = this.params.id;
  let result = Comments.upsert({id:parseInt(id)},data,function(){

  });
  if(result)
    this.response.end('ok\n');
  else
    this.response.end('faid\n');
});

Router.route('collection/:id?',{where:'server'})
.post(function(){
  let data = this.request.body;
  let id   = this.params.id;
  let result = Collections.upsert({id:parseInt(id)},data,function(){

  });
  if(result)
    this.response.end('ok\n');
  else
    this.response.end('faid\n');
});

Router.route('news/:id?',{where:'server'})
.post(function(){
  let data = this.request.body;
  let id   = this.params.id;
  let result = News.upsert({id:parseInt(id)},data,function(){

  });
  if(result)
    this.response.end('ok\n');
  else
    this.response.end('faid\n');
});

Router.route('site/:id?',{where:'server'})
.post(function(){
  let data = this.request.body;
  let id   = this.params.id;
  let result = Sites.upsert({id:parseInt(id)},data,function(){

  });
  if(result)
    this.response.end('ok\n');
  else
    this.response.end('faid\n');
});

Router.route('ad/:id?',{where:'server'})
.post(function(){
  let data = this.request.body;
  let id   = this.params.id;
  let result = Ads.upsert({id:parseInt(id)},data,function(){

  });
  if(result)
    this.response.end('ok\n');
  else
    this.response.end('faid\n');
});

Router.route('category/:id?',{where:'server'})
.post(function(){
  let data = this.request.body;
  let id   = this.params.id;
  let result = Categorys.upsert({id:parseInt(id)},data,function(){

  });
  if(result)
    this.response.end('ok\n');
  else
    this.response.end('faid\n');
});

Router.route('gift/:id?',{where:'server'})
.post(function(){
  let data = this.request.body;
  let id   = this.params.id;
  let result = Gifts.upsert({id:parseInt(id)},data,function(){

  });
  if(result)
    this.response.end('ok\n');
  else
    this.response.end('faid\n');
});

Router.route('university/:id?',{where:'server'})
.post(function(){
  let data = this.request.body;
  let id   = this.params.id;
  let result = Universities.upsert({id:parseInt(id)},data,function(){

  });
  if(result)
    this.response.end('ok\n');
  else
    this.response.end('faid\n');
});

Router.route('college/:id?',{where:'server'})
.post(function(){
  let data = this.request.body;
  let id   = this.params.id;
  let result = Colleges.upsert({id:parseInt(id)},data,function(){

  });
  if(result)
    this.response.end('ok\n');
  else
    this.response.end('faid\n');
});

Router.route('major/:id?',{where:'server'})
.post(function(){
  let data = this.request.body;
  let id   = this.params.id;
  let result = Majors.upsert({id:parseInt(id)},data,function(){

  });
  if(result)
    this.response.end('ok\n');
  else
    this.response.end('faid\n');
});

Router.route('class-code/:id?',{where:'server'})
.post(function(){
  let data = this.request.body;
  let id   = this.params.id;
  let result = ClassCodes.upsert({id:parseInt(id)},data,function(){

  });
  if(result)
    this.response.end('ok\n');
  else
    this.response.end('faid\n');
});

Router.route('coupon-recode/:id?',{where:'server'})
.post(function(){
  let data = this.request.body;
  let id   = this.params.id;
  let result = CouponRecodes.upsert({id:parseInt(id)},data,function(){

  });
  if(result)
    this.response.end('ok\n');
  else
    this.response.end('faid\n');
});

Router.route('coupon/:id?',{where:'server'})
.post(function(){
  let data = this.request.body;
  let id   = this.params.id;
  let result = Coupons.upsert({id:parseInt(id)},data,function(){

  });
  if(result)
    this.response.end('ok\n');
  else
    this.response.end('faid\n');
});

Router.route('refund/:id?',{where:'server'})
.post(function(){
  let data = this.request.body;
  let id   = this.params.id;
  let result = Refunds.upsert({id:parseInt(id)},data,function(){

  });
  if(result)
    this.response.end('ok\n');
  else
    this.response.end('faid\n');
});

Router.route('ordergoods/:id?',{where:'server'})
.post(function(){
  let data = this.request.body;
  let id   = this.params.id;
  let result = OrderGoods.upsert({id:parseInt(id)},data,function(){

  });
  if(result)
    this.response.end('ok\n');
  else
    this.response.end('faid\n');
});

Router.route('partner/:id?',{where:'server'})
.post(function(){
  let data = this.request.body;
  let id   = this.params.id;
  let result = Partners.upsert({id:parseInt(id)},data,function(){

  });
  if(result)
    this.response.end('ok\n');
  else
    this.response.end('faid\n');
});

Router.route('province/:id?',{where:'server'})
.post(function(){
  let data = this.request.body;
  let id   = this.params.id;
  let result = Provinces.upsert({id:parseInt(id)},data,function(){

  });
  if(result)
    this.response.end('ok\n');
  else
    this.response.end('faid\n');
});

Router.route('city/:id?',{where:'server'})
.post(function(){
  let data = this.request.body;
  let id   = this.params.id;
  let result = Cities.upsert({id:parseInt(id)},data,function(){

  });
  if(result)
    this.response.end('ok\n');
  else
    this.response.end('faid\n');
});

Router.route('district/:id?',{where:'server'})
.post(function(){
  let data = this.request.body;
  let id   = this.params.id;
  let result = Districts.upsert({id:parseInt(id)},data,function(){

  });
  if(result)
    this.response.end('ok\n');
  else
    this.response.end('faid\n');
});

Router.route('wallet/:id?',{where:'server'})
.post(function(){
  let data = this.request.body;
  let id   = this.params.id;
  let result = Wallets.upsert({id:parseInt(id)},data,function(){

  });
  if(result)
    this.response.end('ok\n');
  else
    this.response.end('faid\n');
});


Router.route('capital/:id?',{where:'server'})
.post(function(){
  let data = this.request.body;
  let id   = this.params.id;
  let result = Capitals.upsert({id:parseInt(id)},data,function(){

  });
  if(result)
    this.response.end('ok\n');
  else
    this.response.end('faid\n');
});

Router.route('draw/:id?',{where:'server'})
.post(function(){
  let data = this.request.body;
  let id   = this.params.id;
  console.log(data)
  let result = Draws.upsert({id:parseInt(id)},data,function(){

  });
  if(result)
    this.response.end('ok\n');
  else
    this.response.end('faid\n');
});
