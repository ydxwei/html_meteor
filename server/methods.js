Meteor.methods({
  currentUser:function(id){
    if(id){
      let user = Users.findOne({user_base_id:id});
      return {'user':user,'status':'OK',id:id};
    }else{
      return {'error':'id='+id+'用户信息为空','status':'ERROR'};
    }
  },
  loginTokenUpsert:function (token) {
    if(token){
      return LoginToken.upsert({id:parseInt(token['id'])},token);
    }else{
      return false;
    }
  }
});
