import { EJSON } from 'meteor/ejson';

Meteor.publish('user',function(options){
  return Users.find({},options);
});

Meteor.publish('curUser',function(id){
  return Users.find({id:parseInt(id)});
});

Meteor.publish('currentUser',function(id){
  return Users.find({user_base_id:parseInt(id)});
});

Meteor.publish('accessTokens',function(selector){
  return AccessToken.find(selector);
});

Meteor.publish('loginTokens',function(selector,options){
  return LoginToken.find(selector,options);
});
Meteor.publish('provinces',function(){
  var options = {
    fields:{
      id:1,
      name:1,
      code:1
    }
  };
  if(arguments.length > 1){
    for (var key in arguments[1]) {
      if (arguments[1].hasOwnProperty(key)) {
        options[key] = arguments[1][key];
      }
    }
  }
  return Provinces.find(arguments[0],options);
});
Meteor.publish('cities',function(){
  var options = {
    fields:{
      id:1,
      name:1,
      province_code:1,
      code:1
    }
  };
  if(arguments.length > 1){
    for (var key in arguments[1]) {
      if (arguments[1].hasOwnProperty(key)) {
        options[key] = arguments[1][key];
      }
    }
  }
  return Cities.find(arguments[0],options);
});
Meteor.publish('districts',function(){
  var options = {
    fields:{
      id:1,
      name:1,
      city_code:1,
      code:1
    }
  };
  if(arguments.length > 1){
    for (var key in arguments[1]) {
      if (arguments[1].hasOwnProperty(key)) {
        options[key] = arguments[1][key];
      }
    }
  }
  return Districts.find(arguments[0],options);
});
Meteor.publish('sites',function(selector,options){
  Sites._ensureIndex({location:'2dsphere'});
  return Sites.find(selector,options);
});

Meteor.publish('test',function(){
  return {test:1,ok:2,go:3};
});

Meteor.publish('collections',function(selector,options){
  return Collections.find(selector,options);
});

Meteor.publish('gifts',function(selector,options){
  return Gifts.find(selector,options);
});

Meteor.publish('orders',function(selector,options){
  return Orders.find(selector,options);
});

Meteor.publish('orderlist',function(userId,sort,limit){
  let options = {};
  if(typeof(sort) != 'undefined'){
    options['sort'] = sort;
  }

  if(typeof(limit) != 'undefined'){
    options['limit'] = limit
  }
  options['fields'] = {
    id:true,
    merchant:true,
    state:true,
    pay_time:true,
    is_comment:true,
    order_no:true,
    total_fee:true,
    pay_fee:true,
    created_at:true,
    user_id:true
  }
  return Orders.find({user_id:userId},options);
});

Meteor.publish('merchant',function(selector,options){
  for(var i in selector){
    if(i == '$or'){
      for(var j in selector[i]){
        for(var k in selector[i][j]){
          selector[i][j][k] = new RegExp(selector[i][j][k]);
        }
      }
    }
  }
  merchant._ensureIndex({location:'2dsphere'});
  selector.state = 'opened';
  return merchant.find(selector,options);
});

Meteor.publish('allSortMerchant',function(limit){
  selector.state = 'opened';
  return merchant.find({},{limit:limit,sort:{id:-1}});
});

Meteor.publish('sellSortMerchant',function(limit){
  selector.state = 'opened';
  return merchant.find({},{limit:limit,sort:{month_order_count:-1}});
});

Meteor.publish('disSortMerchant',function(limit){
  selector.state = 'opened';
  return merchant.find({},{limit:limit,sort:{id:1}});
});

Meteor.publish('merchantDetail',function(selector,options){
  selector.state = 'opened';
  return merchantDetail.find(selector,options);
});

Meteor.publish('goods',function(selector,options){
  selector.state = 'up';
  return goods.find(selector,options);
})

Meteor.publish('categorys',function(selector,options){
  return Categorys.find(selector,options);
})

Meteor.publish('ads',function(selector,options){
  return Ads.find(selector,options);
});

Meteor.publish('news',function(options){
  return News.find({},options);
});

Meteor.publish('universities',function(){
  var options = {
    fields:{
      id:1,
      name:1,
      district_code:1,
      code:1
    }
  };
  if(arguments.length > 1){
    for (var key in arguments[1]) {
      if (arguments[1].hasOwnProperty(key)) {
        options[key] = arguments[1][key];
      }
    }
  }
  return Universities.find(arguments[0],options);
  //return Universities.find(selector,options);
});

Meteor.publish('colleges',function(){
  var options = {
    fields:{
      id:1,
      name:1,
      university_code:1,
      code:1
    }
  };
  if(arguments.length > 1){
    for (var key in arguments[1]) {
      if (arguments[1].hasOwnProperty(key)) {
        options[key] = arguments[1][key];
      }
    }
  }
  return Colleges.find(arguments[0],options);
});

Meteor.publish('majors',function(){
  var options = {
    fields:{
      id:1,
      name:1,
      college_code:1,
      grade:1,
      code:1
    }
  };
  if(arguments.length > 1){
    for (var key in arguments[1]) {
      if (arguments[1].hasOwnProperty(key)) {
        options[key] = arguments[1][key];
      }
    }
  }
  return Majors.find(arguments[0],options);
});

Meteor.publish('classCodes',function(){
  var options = {
    fields:{
      id:1,
      name:1,
      major_code:1,
      code:1
    }
  };
  if(arguments.length > 1){
    for (var key in arguments[1]) {
      if (arguments[1].hasOwnProperty(key)) {
        options[key] = arguments[1][key];
      }
    }
  }
  return ClassCodes.find(arguments[0],options);
});

Meteor.publish('comments',function(selector,options){
  return Comments.find(selector,options);
});

Meteor.publish('messages',function(options){
  return Messages.find({},options);
});

Meteor.publish('coupons',function(selector,options){
  return Coupons.find(selector,options);
});

Meteor.publish('couponRecodes',function(selector,options){
  return CouponRecodes.find(selector,options);
});

Meteor.publish('orderGoods',function(selector,options){
  return OrderGoods.find(selector,options);
});
