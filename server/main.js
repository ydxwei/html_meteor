import { Meteor } from 'meteor/meteor';

Meteor.startup(() => {
  __meteor_runtime_config__.API_HOST = process.env.API_HOST;
  __meteor_runtime_config__.WEIXIN_AUTH_URL = process.env.WEIXIN_AUTH_URL;
});
