module.exports = {
  servers: {
    one: {
      // TODO: set host address, username, and authentication method
      host: '121.196.233.244',
      username: 'root',
      // pem: './path/to/pem'
       password: 'XHWang3&'
      // or neither for authenticate from ssh-agent
    }
  },
  meteor: {
    // TODO: change app name and path
    name: 'html_meteor',
    path: '../html_meteor',

    servers: {
      one: {},
    },

    buildOptions: {
      serverOnly: true,
      debug: true,
      cleanAfterBuild: true, // default
      // (--architecture--) Builds the server for a different architecture //
      // than your developer machine's architecture. //
      //architecture: 'os.linux.x86_64'
      //buildLocation: '/usr/share/nginx/client', // defaults to /tmp/<uuid>
    },

    env: {
      // TODO: Change to your app's url
      // If you are using ssl, it needs to start with https://
      PORT: 8000,
      ROOT_URL: 'http://user-client.37shenghuo.com',
      MONGO_URL: 'mongodb://localhost:27017/test',
    },

    // ssl: { // (optional)
    //   // Enables let's encrypt (optional)
    //   autogenerate: {
    //     email: 'email.address@domain.com',
    //     // comma seperated list of domains
    //     domains: 'website.com,www.website.com'
    //   }
    // },

    docker: {
      // change to 'kadirahq/meteord' if your app is not using Meteor 1.4
      //image: 'abernix/meteord:base',
      //image:"loongmxbt/meteord:base"
		image:" ulexus/meteor"
      //image:'chriswessels/meteor-tupperware'
       //imagePort: 8080, // (default: 80, some images EXPOSE different ports)
      // (optional) Only used if using your own ssl certificates.
        // Default is "meteorhacks/mup-frontend-server"
      //imageFrontendServer: 'meteorhacks/mup-frontend-server',
      // lets you bind the docker container to a
      // specific network interface (optional)
      //bind: '127.0.0.1',
      // lets you add network connections to perform after run
      // (runs docker network connect <net name> for each network listed here)
      // networks: [
      //   'net1'
      // ]
    },

    // This is the maximum time in seconds it will wait
    // for your app to start
    // Add 30 seconds if the server has 512mb of ram
    // And 30 more if you have binary npm dependencies.
    deployCheckWaitTime: 60,

    // Show progress bar while uploading bundle to server
    // You might need to disable it on CI servers
    enableUploadProgressBar: true
  },

  mongo: {
    port: 27017,
    version: '3.4.1',
    servers: {
      one: {}
    }
  }
};
